import StudentManager.GUI;

import javax.swing.*;
import java.io.IOException;

public class _1512026 {

    public static void main(String[] args) {
        // write your code here
        SwingUtilities.invokeLater(
                new Runnable() {
                    @Override
                    public void run() {
                        GUI gui= null;
                        try {
                            gui = new GUI();
                            gui.setVisible(true);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
    }
}
