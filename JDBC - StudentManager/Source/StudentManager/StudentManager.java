package StudentManager;

import java.io.File;
import java.util.Date;
import java.util.LinkedList;

public class StudentManager {

    //LinkedList<Student> students;
    DataAcess dataAcess;
    Date minDate;

    StudentManager(DataAcess dataAcess){
        this.dataAcess=dataAcess;
        minDate=new Date(0);
    }

    LinkedList<Student> getStudents(){
        LinkedList<Student> students= (LinkedList<Student>) dataAcess.getStudentList();
        return students;
    }

    Student getStudentByID(int id){
        if(id < 1)
            return null;
        return dataAcess.getStudentByID(id);
    }


    public void updateStudent(Student student) {
        if(student==null)
            return;
        dataAcess.updateStudent(student);
    }

    void updateStudentImage(int id,File file){
        if(file==null || id < 1)
            return;
        dataAcess.updateStudentImage(id,file);
    }

    void insertStudent(Student student){
        if(student==null)
            return;
        if(student.getHinhAnh() == null)
            dataAcess.insertStudentWithoutImage(student);
        else
            dataAcess.insertStudent(student);
    }

    LinkedList<Student> findStudentByID(int id){
        if(id < 1)
            return null;
        return (LinkedList)dataAcess.findStudentByID(id);
    }

    LinkedList<Student> findStudent(String name, String note,Date from,Date to){
        if(name==null)
            name="";
        if(note==null)
            note="";
        if(from==null)
            from=minDate;
        if(to==null)
            to=new Date();//today
        return (LinkedList<Student>) dataAcess.findStudent(name,note,from,to);
    }
}
