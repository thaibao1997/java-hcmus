package StudentManager;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class GUI extends JFrame implements ListSelectionListener, MouseListener {

    //main panel components
    private JTable tableStudent;
    private JButton btnAdd;
    private JButton btnUpdate;
    private JButton btnSearch;
    private JButton btnUpdateImage;
    private JImagePanel imagePanel;
    private JTextField txtMaHS;
    private JTextField txtTenHS;
    private JDatePickerImpl txtNgaySinh;
    private JTextArea txtGhiChu;

    //search panel components
    private JTable tableResults;
    private JTextField txtSMaHS;
    private JTextField txtSTenHS;
    private JDatePickerImpl dateNSFrom;
    private JDatePickerImpl dateNSTo;
    private JTextField txtSGhiChu;
    private JButton btnSMaHS;
    private JButton btnSNgaySinh;
    private JButton btnSTenHS;
    private JButton btnSGhiChu;
    private JButton btnSeachAll;

    //
    private JTabbedPane tabbedPane;
    private Font font;
    private StudentManager studentManager;

    public GUI() throws IOException {
        super("Student Manager");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(815, 685);
        font = new Font("monospace", Font.PLAIN, 14);
        setResizable(false);


        JPanel mainPanel = getMainPanel();
        JPanel searchPanel = getSearchPanel();
        tabbedPane = new JTabbedPane();
        tabbedPane.setFont(font);
        tabbedPane.add("Danh Sách Học Sinh", mainPanel);
        tabbedPane.add("Tìm Kiếm Học Sinh", searchPanel);
        tabbedPane.setSelectedIndex(0);
        setContentPane(tabbedPane);

        setVisible(true);

        DBConnectDialog dbDlg = new DBConnectDialog();
        DataAcess dataAcess = dbDlg.showDialog();
        if (dataAcess != null) {
            //tab pane
            studentManager = new StudentManager(dataAcess);
            loadStudentTable();
        } else {
            dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }


    }

    //EVENTS
    @Override
    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        if (listSelectionEvent.getSource().equals(tableStudent.getSelectionModel())) {
            int row = tableStudent.getSelectedRow();
            if (row > -1) {
                row = tableStudent.convertRowIndexToView(row);
                int id = (Integer) tableStudent.getModel().getValueAt(row, 0);
                showStudentDetails(id);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if (mouseEvent.getSource().equals(btnUpdate)) {
            if (txtMaHS.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Hãy Chọn một học sinh để cập nhật");
                return;
            }
            updateStudent();
            loadStudentTable();
            reloadResultsTable();
        } else if (mouseEvent.getSource().equals(btnUpdateImage)) {
            if (txtMaHS.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Hãy Chọn một học sinh để cập nhật");
                return;
            }
            updateStudentImage();
        } else if (mouseEvent.getSource().equals(btnAdd)) {
            AddDialog addDialog = new AddDialog();
            Student student = addDialog.showDialog();
            System.out.println(student);
            if (student != null) {
                studentManager.insertStudent(student);
                loadStudentTable();
            }
        } else if (mouseEvent.getSource().equals(btnSearch)) {
            tabbedPane.setSelectedIndex(1);
        } else if (mouseEvent.getSource().equals(btnSMaHS)) {
            try {
                int mHS = Integer.parseInt(txtSMaHS.getText());
                addStudentListToTable(tableResults, studentManager.findStudentByID(mHS));
            } catch (NumberFormatException ex) {
                clearTable(tableResults);
                JOptionPane.showMessageDialog(null, "Mã Học Sinh Phải Là Một Số");
            }
        } else if (mouseEvent.getSource().equals(tableResults)) {
            Point point = mouseEvent.getPoint();
            if (mouseEvent.getClickCount() == 2) {
                int row = tableResults.rowAtPoint(point);
                if (row > -1) {
                    int id = (Integer) tableResults.getModel().getValueAt(row, 0);
                    showStudentDetails(id);
                }
                tableStudent.clearSelection();
                tabbedPane.setSelectedIndex(0);
            }
        } else if (mouseEvent.getSource().equals(btnSTenHS)) {
            String name = txtSTenHS.getText().trim();
            if (name.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Hãy Nhập Tên Để Tìm Kiếm");
                return;
            }
            addStudentListToTable(tableResults, studentManager.findStudent(name, "", null, null));
        } else if (mouseEvent.getSource().equals(btnSGhiChu)) {
            String note = txtSGhiChu.getText().trim();
            if (note.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Hãy Nhập Chuỗi Để Tìm Kiếm");
                return;
            }
            addStudentListToTable(tableResults, studentManager.findStudent("", note, null, null));
        } else if (mouseEvent.getSource().equals(btnSNgaySinh)) {
            String fromStr = dateNSFrom.getJFormattedTextField().getText().trim();
            String toStr = dateNSTo.getJFormattedTextField().getText().trim();
            if (fromStr.isEmpty() && toStr.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Hãy Nhập Ngày Để Tìm Kiếm");
                return;
            }
            Date dateFrom = Utils.pareDate(fromStr);
            Date dateTo = Utils.pareDate(toStr);
            addStudentListToTable(tableResults, studentManager.findStudent("", "", dateFrom, dateTo));
        } else if (mouseEvent.getSource().equals(btnSeachAll)) {
            String name = txtSTenHS.getText().trim();
            String note = txtSGhiChu.getText().trim();
            String fromStr = dateNSFrom.getJFormattedTextField().getText().trim();
            String toStr = dateNSTo.getJFormattedTextField().getText().trim();
            Date dateFrom = Utils.pareDate(fromStr);
            Date dateTo = Utils.pareDate(toStr);
            addStudentListToTable(tableResults, studentManager.findStudent(name, note, dateFrom, dateTo));
        }
    }


    //PRIVATE FUNCTIONS
    private void reloadResultsTable() {
        int selectedRow = tableResults.getSelectedRow();
        SwingWorker<Void, Void> sw = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                DefaultTableModel tableModel = (DefaultTableModel) tableResults.getModel();
                int rowCount = tableModel.getRowCount();
                LinkedList<Student> studentList = new LinkedList<>();
                for (int i = 0; i < rowCount; i++) {
                    int id = (int) tableModel.getValueAt(i, 0);
                    studentList.add(studentManager.getStudentByID(id));
                }
                addStudentListToTable(tableResults, studentList);
                return null;
            }
        };
        sw.execute();
    }

    private void showStudentDetails(int id) {
        Student student = studentManager.getStudentByID(id);
        if (student != null) {
            txtMaHS.setText(String.valueOf(student.getMHS()));
            txtTenHS.setText(student.getTenHS());
            txtNgaySinh.getJFormattedTextField().setText(Utils.formatDate(student.getNgaySinh()));
            txtGhiChu.setText(student.getGhiChu());
            imagePanel.setImageFromBytes(student.getHinhAnh());
        }
    }

    private void clearTable(JTable table) {
        DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
        tableModel.setRowCount(0);
    }

    private void addStudentListToTable(JTable table, LinkedList<Student> students) {
        if (students == null)
            return;
        clearTable(table);
        DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
        for (Student student : students) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    tableModel.addRow(new Object[]{student.getMHS()
                            , student.getTenHS(), Utils.formatDate(student.getNgaySinh())
                            , student.getGhiChu()});

                }
            });
        }
    }

    private void loadStudentTable() {
        SwingWorker<Void, Void> sw = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                LinkedList<Student> students = studentManager.getStudents();
                addStudentListToTable(tableStudent, students);
                return null;
            }
        };
        sw.execute();
    }

    private void updateStudent() {
        int id = Integer.parseInt(txtMaHS.getText());
        String tenHS = txtTenHS.getText();
        Date ngaySinh = Utils.pareDate(txtNgaySinh.getJFormattedTextField().getText());
        String ghiChu = txtGhiChu.getText();
        if (tenHS.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Tên Học Sinh Không được rỗng");
            return;
        }
        Student student = new Student(id, tenHS, ngaySinh, ghiChu, null);
        studentManager.updateStudent(student);

    }

    private void updateStudentImage() {
        final JFileChooser fc = new JFileChooser();
        FileFilter imageFilter = new FileNameExtensionFilter(
                "Image files", ImageIO.getReaderFileSuffixes());
        fc.setAcceptAllFileFilterUsed(false);
        fc.setFileFilter(imageFilter);
        int returnVal = fc.showOpenDialog(new JPanel());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            int id = Integer.parseInt(txtMaHS.getText());
            studentManager.updateStudentImage(id, file);
            imagePanel.setImageFromPath(file.getPath());
        }
    }

    private JPanel getMainPanel() throws IOException {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(null);

        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder("Danh Sách Học Sinh"));
        panel.setLayout(null);
        panel.setLocation(10, 5);
        panel.setSize(780, 300);

        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.addColumn("Mã Học Sinh");
        tableModel.addColumn("Họ Tên");
        tableModel.addColumn("Ngày Sinh");
        tableModel.addColumn("Ghi Chú");

        tableStudent = new JTable();
        tableStudent.setModel(tableModel);
        tableStudent.setFont(font);
        tableStudent.setAutoCreateRowSorter(true);

        TableRowSorter trs = new TableRowSorter(tableStudent.getModel());
        class IntComparator implements Comparator {
            public int compare(Object o1, Object o2) {
                Integer int1 = Integer.parseInt(o1.toString());
                Integer int2 = Integer.parseInt(o2.toString());
                return int1.compareTo(int2);
            }

            public boolean equals(Object o2) {
                return this.equals(o2);
            }
        }
        trs.setComparator(0, new IntComparator());
        tableStudent.setRowSorter(trs);

        trs = new TableRowSorter(tableStudent.getModel());
        class DateComparator implements Comparator {
            public int compare(Object o1, Object o2) {
                Date date1 = Utils.pareDate(o1.toString());
                Date date2 = Utils.pareDate(o2.toString());
                return date1.compareTo(date2);
            }
            public boolean equals(Object o2) {
                return this.equals(o2);
            }
        }
        trs.setComparator(2, new DateComparator());
        tableStudent.setRowSorter(trs);


        JScrollPane scrollPane = new JScrollPane(tableStudent);
        scrollPane.setSize(760, 275);
        scrollPane.setLocation(10, 15);
        panel.add(scrollPane);
        mainPanel.add(panel);
        Toolkit tk = this.getToolkit();
        Image imgAdd = ImageIO.read(this.getClass().getResourceAsStream("Icons/add.png"));
        btnAdd = new JButton("Thêm HS");
        btnAdd.setLocation(10, 325);
        btnAdd.setSize(150, 50);
        btnAdd.setFont(font);
        btnAdd.setIcon(new ImageIcon(imgAdd));
        mainPanel.add(btnAdd);

        Image imgUpdate = ImageIO.read(this.getClass().getResourceAsStream("Icons/update.png"));
        btnUpdate = new JButton("Cập Nhật");
        btnUpdate.setLocation(10, 400);
        btnUpdate.setSize(150, 50);
        btnUpdate.setFont(font);
        btnUpdate.setIcon(new ImageIcon(imgUpdate));
        mainPanel.add(btnUpdate);

        Image imgSearch = ImageIO.read(this.getClass().getResourceAsStream("Icons/search.png"));
        btnSearch = new JButton("Tìm Kiếm");
        btnSearch.setLocation(10, 475);
        btnSearch.setSize(150, 50);
        btnSearch.setFont(font);
        btnSearch.setIcon(new ImageIcon(imgSearch));
        mainPanel.add(btnSearch);

        JLabel label = new JLabel();
        panel = new JPanel();
        panel.setLocation(170, 320);
        panel.setSize(620, 290);
        panel.setLayout(null);
        panel.setBorder(BorderFactory.createTitledBorder("Thông Tin Chi Tiết"));

        imagePanel = new JImagePanel();
        imagePanel.setLocation(20, 15);
        imagePanel.setSize(180, 240);
        //imagePanel.setImage("./icons/avatar.jpg");
        panel.add(imagePanel);

        label.setText("Hình Ảnh Học Sinh");
        label.setSize(150, 25);
        label.setLocation(20, 260);
        panel.add(label);

        btnUpdateImage = new JButton("Cập Nhật");
        btnUpdateImage.setSize(90, 25);
        btnUpdateImage.setLocation(110, 260);
        panel.add(btnUpdateImage);


        label = new JLabel("Mã Học Sinh: ");
        label.setFont(font);
        label.setLocation(220, 20);
        label.setSize(140, 25);
        panel.add(label);

        txtMaHS = new JTextField();
        txtMaHS.setFont(font);
        txtMaHS.setLocation(320, 20);
        txtMaHS.setSize(280, 25);
        txtMaHS.setEditable(false);
        panel.add(txtMaHS);

        label = new JLabel("Họ Tên: ");
        label.setFont(font);
        label.setLocation(220, 60);
        label.setSize(140, 25);
        panel.add(label);

        txtTenHS = new JTextField();
        txtTenHS.setFont(font);
        txtTenHS.setLocation(320, 60);
        txtTenHS.setSize(280, 25);
        panel.add(txtTenHS);

        label = new JLabel("Ngày Sinh: ");
        label.setFont(font);
        label.setLocation(220, 100);
        label.setSize(140, 25);
        panel.add(label);

        Properties p = new Properties();
        JDatePanelImpl datePanelFrom = new JDatePanelImpl(new UtilDateModel(), p);
        JDatePanelImpl datePanelTo = new JDatePanelImpl(new UtilDateModel(), p);
        DateLabelFormatter dateLabelFormatter = new DateLabelFormatter();

        txtNgaySinh = new JDatePickerImpl(datePanelFrom, dateLabelFormatter);
        txtNgaySinh.setLocation(320, 100);
        txtNgaySinh.setSize(280, 25);
        txtNgaySinh.getJFormattedTextField().setFont(font);
        txtNgaySinh.setShowYearButtons(true);
        panel.add(txtNgaySinh);

        label = new JLabel("Ghi Chú: ");
        label.setFont(font);
        label.setLocation(220, 140);
        label.setSize(140, 25);
        panel.add(label);

        txtGhiChu = new JTextArea();
        txtGhiChu.setFont(font);
        txtGhiChu.setWrapStyleWord(true);
        txtGhiChu.setLineWrap(true);
        scrollPane = new JScrollPane(txtGhiChu);
        scrollPane.setLocation(320, 140);
        scrollPane.setSize(280, 140);
        panel.add(scrollPane);

        mainPanel.add(panel);
        tableStudent.setDefaultEditor(Object.class, null);
        tableStudent.getSelectionModel().addListSelectionListener(this);
        btnUpdate.addMouseListener(this);
        btnUpdateImage.addMouseListener(this);
        btnAdd.addMouseListener(this);
        btnSearch.addMouseListener(this);
        return mainPanel;
    }

    private JPanel getSearchPanel() throws IOException {
        //Search Panel
        JPanel searchPanel = new JPanel();
        searchPanel.setLayout(null);
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder("Kết quả tìm kiếm"));
        panel.setLayout(null);
        panel.setLocation(10, 5);
        panel.setSize(780, 300);

        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.addColumn("Mã Học Sinh");
        tableModel.addColumn("Họ Tên");
        tableModel.addColumn("Ngày Sinh");
        tableModel.addColumn("Ghi Chú");

        tableResults = new JTable();
        tableResults.setModel(tableModel);
        tableResults.setFont(font);
        tableResults.setDefaultEditor(Object.class, null);
        TableRowSorter trs = new TableRowSorter(tableResults.getModel());
        class IntComparator implements Comparator {
            public int compare(Object o1, Object o2) {
                Integer int1 = Integer.parseInt(o1.toString());
                Integer int2 = Integer.parseInt(o2.toString());
                return int1.compareTo(int2);
            }

            public boolean equals(Object o2) {
                return this.equals(o2);
            }
        }
        trs.setComparator(1, new IntComparator());
        tableResults.setRowSorter(trs);

        JScrollPane scrollPane = new JScrollPane(tableResults);
        scrollPane.setLocation(10, 15);
        scrollPane.setSize(760, 275);
        panel.add(scrollPane);
        searchPanel.add(panel);

        panel = new JPanel();
        panel.setBorder(BorderFactory.createTitledBorder("Tiêu Chí tìm kiếm"));
        panel.setLayout(null);
        panel.setLocation(10, 310);
        panel.setSize(780, 300);
        searchPanel.add(panel);

        //maHS
        JLabel label = new JLabel("Mã Học Sinh:");
        label.setLocation(20, 20);
        label.setSize(150, 25);
        label.setFont(font);
        panel.add(label);

        txtSMaHS = new JTextField();
        txtSMaHS.setLocation(170, 20);
        txtSMaHS.setSize(220, 25);
        txtSMaHS.setFont(font);
        panel.add(txtSMaHS);

        btnSMaHS = new JButton("Tìm Kiếm Theo Mã Học Sinh");
        btnSMaHS.setLocation(390, 20);
        btnSMaHS.setSize(260, 25);
        btnSMaHS.setFont(font);
        panel.add(btnSMaHS);

        //txtSTenHS;
        label = new JLabel("Họ Tên Học Sinh:");
        label.setLocation(20, 60);
        label.setSize(150, 25);
        label.setFont(font);
        panel.add(label);

        txtSTenHS = new JTextField();
        txtSTenHS.setLocation(170, 60);
        txtSTenHS.setSize(220, 25);
        txtSTenHS.setFont(font);
        panel.add(txtSTenHS);

        btnSTenHS = new JButton("Tìm Kiếm Theo Tên Học Sinh");
        btnSTenHS.setLocation(390, 60);
        btnSTenHS.setSize(260, 25);
        btnSTenHS.setFont(font);
        panel.add(btnSTenHS);

        //Ghi Chu
        label = new JLabel("Ghi Chú:");
        label.setLocation(20, 100);
        label.setSize(150, 25);
        label.setFont(font);
        panel.add(label);

        txtSGhiChu = new JTextField();
        txtSGhiChu.setLocation(170, 100);
        txtSGhiChu.setSize(220, 25);
        txtSGhiChu.setFont(font);
        panel.add(txtSGhiChu);

        btnSGhiChu = new JButton("Tìm Kiếm Theo Ghi Chú");
        btnSGhiChu.setLocation(390, 100);
        btnSGhiChu.setSize(260, 25);
        btnSGhiChu.setFont(font);
        panel.add(btnSGhiChu);

        //ngaySinh
        label = new JLabel("Ngày Sinh Học Sinh:");
        label.setLocation(20, 140);
        label.setSize(150, 25);
        label.setFont(font);
        panel.add(label);

        label = new JLabel("Từ");
        label.setLocation(75, 170);
        label.setSize(30, 25);
        label.setFont(font);
        panel.add(label);

        label = new JLabel("Đến");
        label.setLocation(235, 170);
        label.setSize(30, 25);
        label.setFont(font);
        panel.add(label);

        Properties p = new Properties();
        JDatePanelImpl datePanelFrom = new JDatePanelImpl(new UtilDateModel(), p);
        JDatePanelImpl datePanelTo = new JDatePanelImpl(new UtilDateModel(), p);
        DateLabelFormatter dateLabelFormatter = new DateLabelFormatter();

        dateNSFrom = new JDatePickerImpl(datePanelFrom, dateLabelFormatter);
        dateNSFrom.setLocation(105, 170);
        dateNSFrom.setSize(125, 25);
        dateNSFrom.getJFormattedTextField().setFont(font);
        dateNSFrom.setShowYearButtons(true);
        panel.add(dateNSFrom);

        dateNSTo = new JDatePickerImpl(datePanelTo, dateLabelFormatter);
        dateNSTo.setLocation(265, 170);
        dateNSTo.setSize(125, 25);
        dateNSTo.getJFormattedTextField().setFont(font);
        dateNSFrom.setShowYearButtons(true);
        panel.add(dateNSTo);

        btnSNgaySinh = new JButton("Tìm Kiếm Theo Ngày Sinh Học Sinh");
        btnSNgaySinh.setLocation(390, 170);
        btnSNgaySinh.setSize(260, 25);
        btnSNgaySinh.setFont(font);
        panel.add(btnSNgaySinh);

        Image imgSearch = ImageIO.read(this.getClass().getResourceAsStream("Icons/search.png"));
        btnSeachAll = new JButton("Tìm kiếm theo tất cả tiêu chí");
        btnSeachAll.setLocation(220, 220);
        btnSeachAll.setSize(280, 50);
        btnSeachAll.setFont(font);
        btnSeachAll.setIcon(new ImageIcon(imgSearch));
        btnSeachAll.setToolTipText("Tìm kiếm theo tất cả các tiêu chí trên trừ mã học sinh");
        panel.add(btnSeachAll);

        btnSMaHS.addMouseListener(this);
        btnSTenHS.addMouseListener(this);
        btnSGhiChu.addMouseListener(this);
        btnSNgaySinh.addMouseListener(this);
        btnSeachAll.addMouseListener(this);
        tableResults.addMouseListener(this);
        return searchPanel;
    }


    //Unused Events
    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
