package StudentManager;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.SQLException;
import java.util.LinkedList;

public class DBConnectDialog extends JDialog implements MouseListener {
    private JTextField txtDBName;
    private JTextField txtUsername;
    private JPasswordField txtPassword;
    private JButton btnConnect;
    private JButton btnCancel;
    private DataAcess dataAcess;

    public DBConnectDialog() {
        super();
        setTitle("Cấu Hình CSDL");
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setModal(true);
        setResizable(false);
        setLayout(null);
        setSize(340, 220);

        Font font = new Font("monospace", Font.PLAIN, 14);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(null);


        JLabel label = new JLabel("Tên CSDL: ");
        label.setLocation(10, 20);
        label.setSize(100, 25);
        label.setFont(font);
        mainPanel.add(label);

        txtDBName = new JTextField();
        txtDBName.setLocation(110, 20);
        txtDBName.setSize(180, 25);
        txtDBName.setFont(font);
        mainPanel.add(txtDBName);

        label = new JLabel("Username: ");
        label.setLocation(10, 60);
        label.setSize(100, 25);
        label.setFont(font);
        mainPanel.add(label);

        txtUsername = new JTextField();
        txtUsername.setLocation(110, 60);
        txtUsername.setSize(180, 25);
        txtUsername.setFont(font);
        mainPanel.add(txtUsername);

        label = new JLabel("Password: ");
        label.setLocation(10, 100);
        label.setSize(100, 25);
        label.setFont(font);
        mainPanel.add(label);

        txtPassword = new JPasswordField();
        txtPassword.setLocation(110, 100);
        txtPassword.setSize(180, 25);
        txtPassword.setFont(font);
        mainPanel.add(txtPassword);

        btnConnect = new JButton("Kết Nối");
        btnConnect.setLocation(110, 140);
        btnConnect.setSize(80, 30);
        btnConnect.setFont(font);
        mainPanel.add(btnConnect);

        btnCancel = new JButton("Thoát");
        btnCancel.setLocation(195, 140);
        btnCancel.setSize(80, 30);
        btnCancel.setFont(font);
        mainPanel.add(btnCancel);

        LinkedList<String> infos =getSavedInfo();
        if(infos != null){
            txtDBName.setText(infos.get(0));
            txtUsername.setText(infos.get(1));
        }


        setContentPane(mainPanel);

        btnConnect.addMouseListener(this);
        btnCancel.addMouseListener(this);
        btnConnect.setDefaultCapable(true);
        //add(mainPanel);


    }

    DataAcess showDialog() {
        this.setVisible(true);
        return dataAcess;
    }

    private void saveInfo(String db,String username){
        File file=new File("info.xml");
        DocumentBuilder buider = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            file.createNewFile();
            buider = factory.newDocumentBuilder();
            Document doc = buider.newDocument();

            Element rootEl=doc.createElement("Record");
            doc.appendChild(rootEl);

            Element dbEl = doc.createElement("DatabaseName");
            dbEl.appendChild(doc.createTextNode(db));
            rootEl.appendChild(dbEl);

            Element usernameEl = doc.createElement("Username");
            usernameEl.appendChild(doc.createTextNode(username));
            rootEl.appendChild(usernameEl);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(file);
            transformer.transform(source, result);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private LinkedList<String> getSavedInfo(){
        File file=new File("info.xml");
        if(!file.exists())return  null;
        try {
            LinkedList<String> res= new LinkedList<>();
            DocumentBuilder buider = null;
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            buider = factory.newDocumentBuilder();
            Document doc = buider.parse(file);
            Element rootEl=doc.getDocumentElement();

            NodeList nodeList= rootEl.getElementsByTagName("DatabaseName");
            res.add(nodeList.item(0).getTextContent());

            NodeList nodeList1= rootEl.getElementsByTagName("Username");
            res.add(nodeList1.item(0).getTextContent());

            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if (mouseEvent.getSource().equals(btnCancel)) {
            dataAcess = null;
            setVisible(false);
            dispose();
        } else if (mouseEvent.getSource().equals(btnConnect)) {
            dataAcess = new DataAcess();
            String db = txtDBName.getText();
            String username = txtUsername.getText();
            String password = String.valueOf(txtPassword.getPassword());

            if (db.isEmpty() || username.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Tên CSDL và Username trên không được rỗng");
                return;
            }

            try {
                dataAcess.connect(db, username, password);
            } catch (SQLException e) {
                if(e.getErrorCode()== 18456)
                    JOptionPane.showMessageDialog(null, "Sai username hoặc password");
                else
                    JOptionPane.showMessageDialog(null, e.getMessage());

                return;
            }
            SwingWorker<Void,Void> swingWorker=new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    saveInfo(db,username);
                    return null;
                }
            };
            swingWorker.execute();
            setVisible(false);
            dispose();
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
