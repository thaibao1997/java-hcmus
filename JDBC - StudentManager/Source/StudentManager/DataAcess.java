package StudentManager;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class DataAcess {
    private final String url="jdbc:sqlserver://localhost:1433;databaseName=";
    private Connection con = null;

    public DataAcess() {

    }

    public void connect(String db,String username,String password) throws SQLException {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(url+db+";",username,password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }catch (SQLException e){
            throw  e;
        }
    }

    List<Student> getStudentList() {
        LinkedList<Student> list = new LinkedList<>();
        if(con==null) return list;
        try {

            Statement st = con.createStatement();
            String strsql = "SELECT MaHS,TenHS,NgaySinh,GhiChu FROM HOCSINH";
            ResultSet rs = st.executeQuery(strsql);
            while (rs.next()) {
                Student student = new Student(rs.getInt("MaHS"),
                        rs.getString("TenHS"), rs.getDate("NgaySinh")
                        , rs.getString("GhiChu"), null);
                list.push(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    Student getStudentByID(int id) {
        if(con==null) return null;
        try {
            String strsql = "SELECT * FROM HOCSINH WHERE MaHS = ?";
            PreparedStatement st = con.prepareStatement(strsql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Student student = new Student(rs.getInt("MaHS"),
                        rs.getString("TenHS"), rs.getDate("NgaySinh")
                        , rs.getString("GhiChu"), rs.getBytes("ExtInfo"));
                return student;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void updateStudent(Student student) {
        if(con==null) return ;
        String strsql = "Update HocSinh SET TenHS = ?,NgaySinh =?,GhiChu=?  WHERE MaHS = ?";
        try {
            PreparedStatement st = con.prepareStatement(strsql);
            st.setString(1, student.getTenHS());
            st.setDate(2, new java.sql.Date(student.getNgaySinh().getTime()));
            st.setString(3, student.getGhiChu());
            st.setInt(4, student.getMHS());
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void updateStudentImage(int id, File file) {
        if(con==null) return;
        String strsql = "Update HocSinh Set ExtInfo = ? where MaHS =?";
        PreparedStatement st = null;

        try {
            st = con.prepareStatement(strsql);
            st.setBlob(1, new FileInputStream(file));
            st.setInt(2, id);
            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertStudent(Student student) {
        if(con==null) return ;
        String strsql = "Insert into HocSinh(TenHS,NgaySinh,GhiChu,ExtInfo) VALUES (?,?,?,?)";
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(strsql);
            st.setString(1, student.getTenHS());
            st.setDate(2, new Date(student.getNgaySinh().getTime()));
            st.setString(3, student.getGhiChu());
            st.setBlob(4,new ByteArrayInputStream(student.getHinhAnh()));
            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertStudentWithoutImage(Student student) {
        if(con==null) return;
        String strsql = "Insert into HocSinh(TenHS,NgaySinh,GhiChu) VALUES (?,?,?)";
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(strsql);
            st.setString(1, student.getTenHS());
            st.setDate(2, new Date(student.getNgaySinh().getTime()));
            st.setString(3, student.getGhiChu());
            st.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    List<Student> findStudentByID(int id) {
        LinkedList<Student> list = new LinkedList<>();
        if(con==null) return list;
        try {
            String strsql = "SELECT MaHS,TenHS,NgaySinh,GhiChu FROM HOCSINH WHERE MaHS = ?";
            PreparedStatement st = con.prepareStatement(strsql);
            st.setInt(1,id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Student student = new Student(rs.getInt("MaHS"),
                        rs.getString("TenHS"), rs.getDate("NgaySinh")
                        , rs.getString("GhiChu"), null);
                list.push(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    List<Student> findStudent(String name, String note, java.util.Date from, java.util.Date to) {
        LinkedList<Student> list = new LinkedList<>();
        if(con==null) return list;

        try {
            String strsql = "SELECT MaHS,TenHS,NgaySinh,GhiChu " +
                    "FROM HOCSINH WHERE TenHS LIKE ? AND GhiChu LIKE ? AND NgaySinh BETWEEN  ? AND ?";
            PreparedStatement st = con.prepareStatement(strsql);
            st.setString(1,"%"+name+"%");
            st.setString(2,"%"+note+"%");
            st.setDate(3,new Date(from.getTime()));
            st.setDate(4,new Date(to.getTime()));
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Student student = new Student(rs.getInt("MaHS"),
                        rs.getString("TenHS"), rs.getDate("NgaySinh")
                        , rs.getString("GhiChu"), null);
                list.push(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }




}
