package StudentManager;


import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

public class JImagePanel extends JPanel {

    private static final String defaultImage = "Icons/default_image.png";
    private Image image;

    public JImagePanel() {
        image = null;
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

    }

    public void setImageFromPath(String path) {
        try {
            image = ImageIO.read(new FileInputStream(path));
            image = image.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_SMOOTH);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.repaint();
    }

    public void setImageFromBytes(byte[] imageByte) {
        try {
            if (imageByte == null) {
                image = ImageIO.read(this.getClass().getResourceAsStream(defaultImage));
                image = image.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_SMOOTH);
                this.repaint();
                return;
            }
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
            image = image.getScaledInstance(this.getWidth(), this.getHeight(), Image.SCALE_SMOOTH);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.repaint();
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, this);
    }

}