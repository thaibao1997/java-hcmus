/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StudentManager;

import java.util.Date;
import java.util.Objects;

/**
 * @author thaibao
 */
public class Student {
    private int mHS;
    private String tenHS;
    private Date ngaySinh;
    private String ghiChu;
    private byte[] hinhAnh;

    public Student() {
        tenHS = null;
        ngaySinh = null;
        ghiChu = null;
        hinhAnh = null;
    }

    public Student(int mHS, String tenHS,Date ngaySinh,String ghiChu,byte[] hinhAnh) {
        this.setMHS(mHS);
        this.setTenHS(tenHS);
        this.setNgaySinh(ngaySinh);
        this.setGhiChu(ghiChu);
        this.setHinhAnh(hinhAnh);
    }

    public int getMHS() {
        return mHS;
    }

    public void setMHS(int mHS) {
        this.mHS = mHS;
    }

    public String getTenHS() {
        return tenHS;
    }

    public void setTenHS(String tenHS) {
        this.tenHS = tenHS;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public byte[] getHinhAnh() {
        return hinhAnh;
    }

    public void setHinhAnh(byte[] hinhAnh) {
        this.hinhAnh = hinhAnh;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(mHS, student.mHS) &&
                Objects.equals(tenHS, student.tenHS) &&
                Objects.equals(ngaySinh, student.ngaySinh) &&
                Objects.equals(ghiChu, student.ghiChu) &&
                Objects.equals(hinhAnh, student.hinhAnh);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mHS, tenHS, ngaySinh, ghiChu, hinhAnh);
    }
}
