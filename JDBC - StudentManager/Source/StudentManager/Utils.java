package StudentManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Utils {
    private final static SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");

     public static Date pareDate(String date)   {
         try {
             return dateFormat.parse(date);
         } catch (ParseException e) {
             //e.printStackTrace();
         }
         return null;
     }

    public static String formatDate(Date date){
         return dateFormat.format(date);
    }

    public static Date clearTimeOfDate(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
