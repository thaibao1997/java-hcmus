package StudentManager;

import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Properties;

public class AddDialog extends JDialog implements MouseListener{
    private JButton btnAdd;
    private JButton btnCancel;
    private JButton btnUpdateImage;
    private JImagePanel imagePanel;
    private JTextField txtTenHS;
    private JDatePickerImpl txtNgaySinh;
    private JTextArea txtGhiChu;

    private Student student;
    private byte[] image;

    public AddDialog() {
        super();
        setTitle("Add New Student");
        setLayout(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setModal(true);
        setResizable(false);
        setSize(630,330);

        Font font = new Font("monospace", Font.PLAIN, 14);
        JPanel panel = new JPanel();
        panel.setLayout(null);

        JLabel label = new JLabel();

        imagePanel = new JImagePanel();
        imagePanel.setLocation(20, 15);
        imagePanel.setSize(180, 240);
        //imagePanel.setImage("icons/avatar.jpg");
        panel.add(imagePanel);

        label.setText("Hình Ảnh Học Sinh");
        label.setSize(150, 25);
        label.setLocation(20, 260);
        panel.add(label);

        btnUpdateImage = new JButton("Chọn File");
        btnUpdateImage.setSize(90, 25);
        btnUpdateImage.setLocation(110, 260);
        panel.add(btnUpdateImage);

        label = new JLabel("Họ Tên: ");
        label.setFont(font);
        label.setLocation(220, 20);
        label.setSize(140, 25);
        panel.add(label);

        txtTenHS = new JTextField();
        txtTenHS.setFont(font);
        txtTenHS.setLocation(320, 20);
        txtTenHS.setSize(280, 25);
        panel.add(txtTenHS);

        label = new JLabel("Ngày Sinh: ");
        label.setFont(font);
        label.setLocation(220, 60);
        label.setSize(140, 25);
        panel.add(label);

        Properties p = new Properties();
        JDatePanelImpl datePanelFrom = new JDatePanelImpl(new UtilDateModel(), p);
        JDatePanelImpl datePanelTo = new JDatePanelImpl(new UtilDateModel(), p);
        DateLabelFormatter dateLabelFormatter = new DateLabelFormatter();

        txtNgaySinh = new JDatePickerImpl(datePanelFrom, dateLabelFormatter);
        txtNgaySinh.setLocation(320, 60);
        txtNgaySinh.setSize(280, 25);
        txtNgaySinh.getJFormattedTextField().setFont(font);
        txtNgaySinh.setShowYearButtons(true);
        panel.add(txtNgaySinh);

        label = new JLabel("Ghi Chú: ");
        label.setFont(font);
        label.setLocation(220, 100);
        label.setSize(140, 25);
        panel.add(label);

        txtGhiChu = new JTextArea();
        txtGhiChu.setFont(font);
        txtGhiChu.setWrapStyleWord(true);
        txtGhiChu.setLineWrap(true);
        JScrollPane scrollPane = new JScrollPane(txtGhiChu);
        scrollPane.setLocation(320, 100);
        scrollPane.setSize(280, 140);
        panel.add(scrollPane);

        btnAdd=new JButton("Thêm");
        btnAdd.setSize(150,25);
        btnAdd.setLocation(320,250);
        btnAdd.setFont(font);
        panel.add(btnAdd);

        btnCancel=new JButton("Hủy");
        btnCancel.setSize(120,25);
        btnCancel.setLocation(480,250);
        btnCancel.setFont(font);
        panel.add(btnCancel);

        setContentPane(panel);

        btnAdd.addMouseListener(this);
        btnUpdateImage.addMouseListener(this);
        btnCancel.addMouseListener(this);
    }

    Student showDialog() {
        setVisible(true);
        return student;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if(mouseEvent.getSource().equals(btnAdd)){
            String tenHS = txtTenHS.getText();
            if (tenHS.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Tên Học Sinh Không được rỗng");
                return;
            }
            String nsStr=txtNgaySinh.getJFormattedTextField().getText();
            if (nsStr.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Ngày Sinh Học Sinh Không được rỗng");
                return;
            }
            Date ngaySinh = Utils.pareDate(nsStr);
            String ghiChu = txtGhiChu.getText();
            student = new Student(-1,tenHS, ngaySinh, ghiChu, image);
            setVisible(false);
            dispose();
        }
        else if(mouseEvent.getSource().equals(btnUpdateImage)){
            updateStudentImage();
        }
        else if(mouseEvent.getSource().equals(btnCancel)){
            student=null;
            setVisible(false);
            dispose();
        }
    }

    void updateStudentImage() {
        final JFileChooser fc = new JFileChooser();
        FileFilter imageFilter = new FileNameExtensionFilter(
                "Image files", ImageIO.getReaderFileSuffixes());
        fc.setAcceptAllFileFilterUsed(false);
        fc.setFileFilter(imageFilter);
        int returnVal = fc.showOpenDialog(new JPanel());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            imagePanel.setImageFromPath(file.getPath());
            Path fileLocation = Paths.get(file.getPath());
            try {
                image = Files.readAllBytes(fileLocation);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
