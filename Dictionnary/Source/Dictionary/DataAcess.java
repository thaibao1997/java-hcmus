package Dictionary;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;


public class DataAcess {
    private static final String resourceFolder = "resource/";
    private static final String anhVietFile = resourceFolder+ "Anh_Viet.xml";
    private static final String vietAnhFile = resourceFolder+ "Viet_Anh.xml";
    private static final String fwordsFile = resourceFolder+ "FWord.xml";
    private static final String recordsFile = resourceFolder+ "Record.xml";


    public static final int ANH_VIET= 0;
    public static final int VIET_ANH= 1;
    private DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    DataAcess(){

    }

    TreeMap<String ,String> readWords(int mode){
        if(mode < 0 || mode > 1)
            return null;
        TreeMap<String ,String> list=new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        File f= mode == ANH_VIET ?new File(anhVietFile) : new File(vietAnhFile);
        DocumentBuilder buider = null;
        try {
            buider = factory.newDocumentBuilder();
            Document doc = buider.parse(f);
            Element anhViet = doc.getDocumentElement();
            NodeList wordList = anhViet.getElementsByTagName("record");
            for(int i=0;i< wordList.getLength();i++) {
                Node node = wordList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element word = (Element) node;
                    String tu = word.getElementsByTagName("word").item(0).getTextContent();
                    String nghia = word.getElementsByTagName("meaning").item(0).getTextContent();
                    list.put(tu,nghia);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    LinkedList<FWord> getFWords(){
        LinkedList<FWord> list=new LinkedList<>();
        File f = new File(fwordsFile);
        if(!f.exists()){
            generateFwordsFile();
            return list;
        }
        DocumentBuilder buider = null;
        try {
            buider = factory.newDocumentBuilder();
            Document doc = buider.parse(f);
            Element rootEl = doc.getDocumentElement();
            NodeList wordList = rootEl.getElementsByTagName("word");
            for(int i=0;i< wordList.getLength();i++) {
                Node node = wordList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element word = (Element) node;
                    String sType=word.getAttribute("type");
                    list.push(new FWord(word.getTextContent(),DictionaryType.fromString(sType)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    void saveFWord(FWord fWord){
        File f = new File(fwordsFile);
        if(!f.exists()){
            generateFwordsFile();
        }
        DocumentBuilder buider = null;
        try {
            buider = factory.newDocumentBuilder();
            Document doc = buider.parse(f);
            Element rootEl = doc.getDocumentElement();
            Element word = doc.createElement("word");
            word.setAttribute("type",fWord.getType().toString());
            word.setTextContent(fWord.getWord());
            rootEl.appendChild(word);
            saveXMLFile(doc,f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void saveLookupRecord(String word,DictionaryType type){
        File f = new File(recordsFile);
        if(!f.exists()){
            generateRecordsFile();
        }
        DocumentBuilder buider = null;
        try {
            String now=Utils.formatDate(new Date());
            buider = factory.newDocumentBuilder();
            Document doc = buider.parse(f);
            Element rootEl = doc.getDocumentElement();

            NodeList records= rootEl.getElementsByTagName("record");
            boolean found=false;
            for (int i=0;i<records.getLength();i++){
                Node node= records.item(i);
                if(node.getNodeType() == Node.ELEMENT_NODE){
                    Element record = (Element) node;
                    String word_=record.getTextContent();
                    String date_=record.getAttribute("date");
                    String type_=record.getAttribute("type");
                    int time_=Integer.parseInt(record.getAttribute("time"));
                    if( date_.compareTo(now)==0  &&  type_.equals(type) && word_.compareTo(word)==0 ){
                        found=true;
                        time_++;
                        record.setAttribute("time",String.valueOf(time_));
                    }
                }
            }
            String stype=type.equals(DictionaryType.ANH_VIET) ? "A-V" : "V-A";
            if(!found) {
                Element record = doc.createElement("record");
                record.setAttribute("date", now);
                record.setAttribute("type", stype);
                record.setAttribute("time", "1");
                record.setTextContent(word);
                rootEl.appendChild(record);
            }
            saveXMLFile(doc,f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    HashMap<RWord,Integer> getRWords(){
        HashMap<RWord,Integer> map=new HashMap<>();
        File f = new File(recordsFile);
        if(!f.exists()){
            generateRecordsFile();
            return  map;
        }
        DocumentBuilder buider = null;
        try {
            buider = factory.newDocumentBuilder();
            Document doc = buider.parse(f);
            Element rootEl = doc.getDocumentElement();
            NodeList wordList = rootEl.getElementsByTagName("record");
            for(int i=0;i< wordList.getLength();i++) {
                Node node = wordList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element word = (Element) node;
                    String stype= word.getAttribute("type");

                    map.put(new RWord(word.getTextContent()
                            ,DictionaryType.fromString(stype)
                            ,Utils.pareDate(word.getAttribute("date"))),
                            Integer.parseInt(word.getAttribute("time")));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    private void saveXMLFile(Document doc,File f) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(f);
        transformer.transform(source, result);
    }

    private void generateFwordsFile(){
        File fFile=new File(fwordsFile);
        try {
            fFile.createNewFile();
            DocumentBuilder buider = factory.newDocumentBuilder();
            Document doc = buider.newDocument();
            buider = factory.newDocumentBuilder();
            Node rootEl = doc.createElement("Fwords");
            doc.appendChild(rootEl);
            saveXMLFile(doc,fFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void generateRecordsFile(){
        File rFile=new File(recordsFile);
        try {
            rFile.createNewFile();
            DocumentBuilder buider = factory.newDocumentBuilder();
            Document doc = buider.newDocument();
            buider = factory.newDocumentBuilder();
            Node rootEl = doc.createElement("Records");
            doc.appendChild(rootEl);
            saveXMLFile(doc,rFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
