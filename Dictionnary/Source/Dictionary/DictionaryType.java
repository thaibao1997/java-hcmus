package Dictionary;

public enum DictionaryType {
    ANH_VIET("A-V"),
    VIET_ANH("V-A");

    private String str;
    DictionaryType(String str){
        this.str=str;
    }
    @Override
    public String toString() {
        return str;
    }


    public static DictionaryType fromString(String str){
        if (str.equals("A-V"))
            return ANH_VIET;
        if (str.equals("V-A"))
            return VIET_ANH;
        return null;
    }
}
