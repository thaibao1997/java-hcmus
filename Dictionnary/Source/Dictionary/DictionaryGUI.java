package Dictionary;

import org.jdatepicker.impl.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.util.Comparator;
import java.util.Properties;


public class DictionaryGUI extends JFrame{

    private JComboBox cbbType;
    private JButton btnAddFWord;
    private JList listWord;
    private DefaultListModel listModel;
    private JTextArea txtMeaning;
    private DictionaryController controller;
    private JTextField edtLookup;
    private JButton btnLookup;
    private JComboBox cbbSort;
    private JList listFWord;
    private JDatePickerImpl dateFrom;
    private JDatePickerImpl dateTo;
    private JButton btnTK;
    private JTable tableTK;


    public DictionaryGUI(){
        super("Dictionary");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {

        }
        setSize(930,650);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JButton button;
        JPanel panel=new JPanel();
        panel.setLayout(null);

        Font font=new Font("monospace", Font.PLAIN, 14);

        JLabel label=new JLabel("Loại Từ Điển");
        label.setFont(font);
        label.setSize(90,25);
        label.setLocation(10,10);
        panel.add(label);


        cbbType=new JComboBox();
        cbbType.setFont(font);
        cbbType.setLocation(100,10);
        cbbType.setSize(175,25);
        panel.add(cbbType);

        edtLookup=new JTextField();
        edtLookup.setFont(font);
        edtLookup.setLocation(285,10);
        edtLookup.setSize(250,25);
        edtLookup.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        panel.add(edtLookup);

        btnLookup = new JButton("Look Up");
        btnLookup.setFont(font);
        btnLookup.setLocation(535,10);
        btnLookup.setSize(100,25);
        panel.add(btnLookup);

        btnAddFWord=new JButton("Thêm vào Yêu Thích");
        btnAddFWord.setLocation(755,10);
        btnAddFWord.setSize(150,25);
        panel.add(btnAddFWord);



        listWord=new JList();
        listWord.setFont(font);
        JScrollPane scrollList=new JScrollPane(listWord);
        scrollList.setLocation(10,50);
        scrollList.setSize(265,550);
        panel.add(scrollList);



        txtMeaning=new JTextArea();
        txtMeaning.setFont(font);
        txtMeaning.setEditable(false);
        txtMeaning.setLineWrap(true);
        txtMeaning.setWrapStyleWord(true);
        JScrollPane scrollPane=new JScrollPane(txtMeaning);
        scrollPane.setLocation(285,50);
        scrollPane.setSize(620,255);
        panel.add(scrollPane);

        JPanel fPanel=new JPanel();
        fPanel.setBorder(BorderFactory.createTitledBorder("Danh Sách Từ Yêu Thích"));
        fPanel.setLayout(null);
        fPanel.setLocation(285,310);
        fPanel.setSize(220,290);

        label=new JLabel("Sắp Xếp");
        label.setFont(font);
        label.setLocation(10,15);
        label.setSize(75,25);
        fPanel.add(label);

        cbbSort=new JComboBox();
        cbbSort.setLocation(70,15);
        cbbSort.setFont(font);
        cbbSort.setSize(130,25);

        listFWord=new JList();
        listFWord.setFont(font);
        JScrollPane fScoll=new JScrollPane(listFWord);
        fScoll.setLocation(10,45);
        fScoll.setSize(200,235);

        fPanel.add(cbbSort);
        fPanel.add(fScoll);
        panel.add(fPanel);

        fPanel=new JPanel();
        fPanel.setBorder(BorderFactory.createTitledBorder("Thống Kê"));
        fPanel.setLayout(null);
        fPanel.setLocation(510,310);
        fPanel.setSize(400,290);

        Properties p = new Properties();
        JDatePanelImpl datePanelFrom=new JDatePanelImpl(new UtilDateModel(), p);
        JDatePanelImpl datePanelTo=new JDatePanelImpl(new UtilDateModel(), p);
        DateLabelFormatter dateLabelFormatter=new DateLabelFormatter();

        label=new JLabel("Từ");
        label.setLocation(10,20);
        label.setSize(30,25);
        label.setFont(font);
        fPanel.add(label);

        dateFrom=new JDatePickerImpl(datePanelFrom,dateLabelFormatter);
        dateFrom.setSize(105,25);
        dateFrom.setLocation(35,20);
        fPanel.add(dateFrom);


        label=new JLabel("Đến");
        label.setLocation(150,20);
        label.setSize(30,25);
        label.setFont(font);
        fPanel.add(label);

        dateTo=new JDatePickerImpl(datePanelTo,dateLabelFormatter);
        dateTo.setLocation(180,20);
        dateTo.setSize(105,25);
        fPanel.add(dateTo);

        btnTK=new JButton("Thống Kê");
        btnTK.setFont(font);
        btnTK.setLocation(290,20);
        btnTK.setSize(100,25);
        fPanel.add(btnTK);

        DefaultTableModel tableModel=new DefaultTableModel();
        tableModel.addColumn("Từ");
        tableModel.addColumn("Số Lần Tra Cứu");
        tableModel.addColumn("Loại");



        tableTK = new JTable();
        tableTK.setAutoCreateRowSorter(true);
        tableTK.setModel(tableModel);
        tableTK.setDefaultEditor(Object.class, null);
        tableTK.setFont(font);
        JScrollPane tkScrollP=new JScrollPane(tableTK);
        tkScrollP.setLocation(10,50);
        tkScrollP.setSize(380,230);
        fPanel.add(tkScrollP);



        panel.add(fPanel);



        listModel=new DefaultListModel();
        listWord.setModel(listModel);

        controller=new DictionaryController(this);

        listFWord.addListSelectionListener(controller);
        listWord.addListSelectionListener(controller);

        cbbType.addItem(DictionaryController.comboboxTypeString[0]);
        cbbType.addItem(DictionaryController.comboboxTypeString[1]);
        cbbType.addActionListener(controller);

        cbbSort.addItem(DictionaryController.comboboxSortString[0]);
        cbbSort.addItem(DictionaryController.comboboxSortString[1]);
        cbbSort.addActionListener(controller);

        btnAddFWord.addMouseListener(controller);
        btnTK.addMouseListener(controller);

        btnLookup.addMouseListener(controller);
        edtLookup.addKeyListener(controller);
        //edtLookup.getDocument().addDocumentListener(controller);
        tableTK.getSelectionModel().addListSelectionListener(controller);

        TableRowSorter trs = new TableRowSorter(tableModel);
        class IntComparator implements Comparator {
            public int compare(Object o1, Object o2) {
                Integer int1 = Integer.parseInt(o1.toString());
                Integer int2 = Integer.parseInt(o2.toString());
                return int1.compareTo(int2);
            }

            public boolean equals(Object o2) {
                return this.equals(o2);
            }
        }

        trs.setComparator(1, new IntComparator());
        tableTK.setRowSorter(trs);

        setContentPane(panel);
        loadDictionary();


    }

    public void loadDictionary(){
        txtMeaning.setText("Loading Dictionaries...");
        edtLookup.setEditable(false);
        SwingWorker<Void,Void> sw=new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                controller.loadComponent();
                return null;
            }
        };
        sw.execute();
    }

    public JDatePickerImpl getDateFrom() {
        return dateFrom;
    }

    public JDatePickerImpl getDateTo() {
        return dateTo;
    }

    public JButton getBtnTK() {
        return btnTK;
    }

    public JTable getTableTK() {
        return tableTK;
    }

    public JComboBox getCbbType() {
        return cbbType;
    }

    public JButton getBtnAddFWord() {
        return btnAddFWord;
    }

    public JList getListWord() {
        return listWord;
    }

    public DefaultListModel getListModel() {
        return listModel;
    }

    public JTextArea getTxtMeaning() {
        return txtMeaning;
    }

    public DictionaryController getController() {
        return controller;
    }

    public JTextField getEdtLookup() {
        return edtLookup;
    }

    public JButton getBtnLookup() {
        return btnLookup;
    }

    public JComboBox getCbbSort() {
        return cbbSort;
    }

    public JList getListFWord() {
        return listFWord;
    }
}
