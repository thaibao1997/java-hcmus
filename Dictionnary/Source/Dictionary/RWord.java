package Dictionary;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class RWord {
    private Date date;
    private String word;
    private DictionaryType type;

    public RWord(String word,DictionaryType type,Date date){
        this.word=word;
        this.type=type;
        this.date = Utils.clearTimeOfDate(date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RWord rWord = (RWord) o;
        return  Objects.equals(word, rWord.word) &&
                Objects.equals(type, rWord.type) &&
                date.equals(rWord.date) ;
    }

    @Override
    public int hashCode() {

        return Objects.hash(date, word, type);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public DictionaryType getType() {
        return type;
    }

    public void setType(DictionaryType type) {
        this.type = type;
    }
}
