package Dictionary;

import org.jdatepicker.impl.JDatePickerImpl;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.Position;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DictionaryController implements ListSelectionListener,ActionListener,MouseListener,KeyListener{
    public static final String[] comboboxTypeString= {"Anh-Việt","Việt-Anh"};
    public static final String[] comboboxSortString= {"A-Z","Z-A"};


    private DictionaryGUI gui;
    private DataAcess dataAcess;
    private TreeMap<String ,String>  anhViet,vietAnh;
    private LinkedList<FWord> fList;
    private HashMap<RWord,Integer> rMap;

    public DictionaryController(DictionaryGUI gui){
        this.gui=gui;
        dataAcess=new DataAcess();
        anhViet=new TreeMap<> ();
        vietAnh=new TreeMap<>();
        fList=new LinkedList<>();
        rMap=new HashMap<>();
    }

    public void loadComponent(){
        anhViet=dataAcess.readWords(DataAcess.ANH_VIET);
        vietAnh=dataAcess.readWords(DataAcess.VIET_ANH);
        fList = dataAcess.getFWords();
        rMap= dataAcess.getRWords();

        JList wordsList=gui.getListWord();
        JList fwordsList=gui.getListFWord();
        JTextField edtLookup=gui.getEdtLookup();
        JTextArea txtMeaning=gui.getTxtMeaning();

        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                wordsList.setListData(anhViet.keySet().toArray());
                fwordsList.setListData(fList.toArray());
                txtMeaning.setText("");
                edtLookup.setEditable(true);
                edtLookup.requestFocus();
                edtLookup.setCaretPosition(0);
            }
        } );
    }

    private void lookupWord(){
        JTextField edtLookup=gui.getEdtLookup();
        JComboBox cbbType=gui.getCbbType();
        JList wordsList=gui.getListWord();
        JTextArea txtMeaning=gui.getTxtMeaning();
        String text=edtLookup.getText().toLowerCase().trim();
        TreeMap dictionary;
        DictionaryType type;
        type = cbbType.getSelectedIndex() == 0 ? DictionaryType.ANH_VIET : DictionaryType.VIET_ANH;
        dictionary = cbbType.getSelectedIndex() == 0 ? anhViet :vietAnh;
        boolean res=false;
        res=dictionary.containsKey(text);
        int index= wordsList.getNextMatch(text,0, Position.Bias.Forward);
        if(!res) {
            if (index == -1) {
                txtMeaning.setText("Không Tìm Thấy Từ - Not Found");
                wordsList.clearSelection();
                return;
            }
        }
        if (index != -1) {
            wordsList.setSelectedIndex(index);
            wordsList.ensureIndexIsVisible(wordsList.getSelectedIndex());
            if (res)
                saveLookupRecord(wordsList.getSelectedValue().toString(), type);
        }
    }
    private void lookupWord(String word,DictionaryType type){
        JList wordsList=gui.getListWord();
        TreeMap<String,String> dictionary=null;
        int cbbSelectedIndex=0;
        if(type == DictionaryType.ANH_VIET){
            dictionary=anhViet;
        }
        else {
            dictionary = vietAnh;
            cbbSelectedIndex=1;
        }
        Object[] keySet=dictionary.keySet().toArray();
        int index= Arrays.asList(keySet).indexOf(word);
        if(index != -1) {
            JComboBox cbbType=gui.getCbbType();
            cbbType.setSelectedIndex(cbbSelectedIndex);
            wordsList.setListData(keySet);
            wordsList.setSelectedIndex(index);
            wordsList.ensureIndexIsVisible(wordsList.getSelectedIndex());
        }
    }

    private void saveLookupRecord(String word,DictionaryType type){
        SwingWorker<Void,Void> sw=new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                dataAcess.saveLookupRecord(word,type);
                RWord rWord=new RWord(word,type,new Date());
                if(rMap.containsKey(rWord)){
                    int time=rMap.get(rWord)+1;
                    rMap.put(rWord,time);
                }else{
                    rMap.put(rWord,1);
                }
                return null;
            }
        };
        sw.execute();
    }

    private HashMap<FWord,Integer> getTkRWords(Date from,Date to){
        HashMap<FWord,Integer> tkMap=new HashMap<>();
        Object[] arr=rMap.keySet().toArray();
        for(int i=0;i<arr.length;i++){
            RWord rWord=(RWord)arr[i];
            Date rDate=rWord.getDate();
            if(rDate.compareTo(to) <=0 && rDate.compareTo(from) >= 0){
                FWord fWord=new FWord(rWord.getWord(),rWord.getType());
                if(tkMap.containsKey(fWord)){
                    tkMap.put(fWord, tkMap.get(fWord) + rMap.get(rWord));
                }else {
                    tkMap.put(fWord, rMap.get(rWord));
                }
            }
        }
        return tkMap;
    }

    private void addTkTableResults(HashMap tkMap){
        Object[] arr=tkMap.keySet().toArray();
        JTable table=gui.getTableTK();
        DefaultTableModel tableModel=(DefaultTableModel)table.getModel();
        tableModel.setRowCount(0);
        for(int i=0;i<arr.length;i++){
            FWord fWord=(FWord)arr[i];
            LinkedList<String> list=new LinkedList<>();
            list.add(fWord.getWord());
            list.add(tkMap.get(fWord).toString());
            list.add(fWord.getType().toString());
            tableModel.addRow(list.toArray());
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        JList wordsList=gui.getListWord();
        JList fwordsList=gui.getListFWord();
        JTable table=gui.getTableTK();
        if(listSelectionEvent.getSource()==wordsList){
            JTextArea txtMeaning=gui.getTxtMeaning();
            JComboBox cbbType=gui.getCbbType();
            String selected= (String)wordsList.getSelectedValue();
            TreeMap dictionary= cbbType.getSelectedIndex() == 0? anhViet : vietAnh;
            if(selected != null){
                txtMeaning.setText((String) dictionary.get(selected));
                txtMeaning.setCaretPosition(0);
            }
        }else if(listSelectionEvent.getSource() == fwordsList){
            FWord fWord=(FWord)fwordsList.getSelectedValue();
            if(fWord != null){
                lookupWord(fWord.getWord(),fWord.getType());
            }
            fwordsList.clearSelection();
        }else if(listSelectionEvent.getSource() == table.getSelectionModel()){
            int row= table.getSelectedRow();
            if(row >= 0 && row < table.getModel().getRowCount()) {
                row = table.convertRowIndexToModel(row);
                DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
                String word = (String) tableModel.getValueAt(row, 0);
                String type = (String) tableModel.getValueAt(row, 2);
                lookupWord(word, DictionaryType.fromString(type));
            }
            //table.clearSelection();
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        JComboBox cbbType=gui.getCbbType();
        JComboBox cbbSort=gui.getCbbSort();
        if(actionEvent.getSource() == cbbType){
            JList wordsList=gui.getListWord();
            if(cbbType.getSelectedIndex() == 0)
                wordsList.setListData(anhViet.keySet().toArray());
            else
                wordsList.setListData(vietAnh.keySet().toArray());
        }else if(actionEvent.getSource() == cbbSort){
            JList fwordsList = gui.getListFWord();
            if(cbbSort.getSelectedIndex() == 0){
                Collections.sort(fList,
                        (fWord1,fWord2) ->fWord1.getWord().toLowerCase().compareTo(fWord2.getWord().toLowerCase()));
            }else {
                Collections.sort(fList,
                        (fWord1,fWord2) ->fWord2.getWord().toLowerCase().compareTo(fWord1.getWord().toLowerCase()));
            }
            fwordsList.setListData(fList.toArray());
        }
    }


    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        JButton btnLookup = gui.getBtnLookup();
        JButton btnAddFWord = gui.getBtnAddFWord();
        JButton btnTK = gui.getBtnTK();

        if(mouseEvent.getSource() == btnLookup){
            lookupWord();
        }
        else if(mouseEvent.getSource() == btnAddFWord){
            JList wordsList=gui.getListWord();
            Object obj= wordsList.getSelectedValue();
            if(obj != null){
                JComboBox cbbType=gui.getCbbType();
                String word=obj.toString();
                DictionaryType type;
                type = cbbType.getSelectedIndex() == 0 ? DictionaryType.ANH_VIET : DictionaryType.VIET_ANH;
                FWord fWord=new FWord(obj.toString(),type);
                if(!fList.contains(fWord)){
                    fList.push(fWord);
                    JList fwordsList=gui.getListFWord();
                    fwordsList.setListData(fList.toArray());
                    dataAcess.saveFWord(fWord);
                }
            }
        }else if(mouseEvent.getSource() == btnTK){
            JDatePickerImpl dateFrom=gui.getDateFrom();
            JDatePickerImpl dateTo=gui.getDateTo();
            JTable tableTk=gui.getTableTK();
            SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
            Date from= null;
            Date to= null;
            String strDateFrom=dateFrom.getJFormattedTextField().getText();
            String strDateTo=dateTo.getJFormattedTextField().getText();
            if(strDateFrom.equals("") || strDateTo.equals("")){
                JOptionPane.showMessageDialog(null,"Hãy Chọn ngày");
                return;
            }
            try {
                from=dateFormat.parse(strDateFrom);
                to=dateFormat.parse(strDateTo);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(to.before(from)){
                JOptionPane.showMessageDialog(null,"Ngày đến phải sau ngày từ");
                return;
            }
            HashMap<FWord,Integer> tkMap=getTkRWords(from,to);
            addTkTableResults(tkMap);
        }
    }



    @Override
    public void keyPressed(KeyEvent keyEvent) {
        JTextField edtLookup = gui.getEdtLookup();
        if(keyEvent.getSource() == edtLookup && keyEvent.getKeyCode() == KeyEvent.VK_ENTER){
            lookupWord();
        }

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}

    /*@Override
    public void insertUpdate(DocumentEvent documentEvent) {
        JTextField edtLookup = gui.getEdtLookup();
        if(documentEvent.getDocument() == edtLookup.getDocument()){
            lookupWord();
        }
    }

    @Override
    public void removeUpdate(DocumentEvent documentEvent) {
        JTextField edtLookup = gui.getEdtLookup();
        if(documentEvent.getDocument() == edtLookup.getDocument()){
            lookupWord();
        }
    }

    @Override
    public void changedUpdate(DocumentEvent documentEvent) {
        JTextField edtLookup = gui.getEdtLookup();
        if(documentEvent.getDocument() == edtLookup.getDocument()){
            lookupWord();
        }
    }*/
