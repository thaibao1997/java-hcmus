package Dictionary;

import java.util.Objects;

public class FWord {
    private String word;
    private DictionaryType type;

    public FWord(String word,DictionaryType type){
        this.word=word;
        this.type=type;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public DictionaryType getType() {
        return type;
    }

    public void setType(DictionaryType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return word + "  ("+type+")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FWord fWord = (FWord) o;
        return Objects.equals(word, fWord.word) &&
                Objects.equals(type, fWord.type);
    }

    @Override
    public int hashCode() {

        return Objects.hash(word, type);
    }
}
