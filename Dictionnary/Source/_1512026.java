import Dictionary.*;
import javax.swing.*;

public class _1512026 {
    public static void main(String[] args){
        SwingUtilities.invokeLater(
                new Runnable() {
                    @Override
                    public void run() {
                        DictionaryGUI gui=new DictionaryGUI();
                        gui.setVisible(true);
                    }
                }
        );
    }
}
