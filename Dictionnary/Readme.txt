﻿- Thông Tin Sinh Viên:
	Họ Tên: Lương Thái Bảo
	MSSV:	1512026
	E-mail:	thaibao1997@gmail.com
- Hướng Dẫn Biên Dịch Và Chạy Mã Nguồn:
	B1: Copy folder "resource" tại thư mục "Release" vào thư mục "Source"
	B2: Copy file "jdatepicker.jar" tại thư mục "Libs" vào thư mục "Source"
	B3: Mở terminal(command prompt) tại thư mục "Source" và chạy lần lượt 2 lệnh:
		javac -encoding utf-8 -cp .;jdatepicker.jar  _1512026.java
		java -cp .;jdatepicker.jar  _1512026
		