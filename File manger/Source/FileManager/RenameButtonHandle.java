package FileManager;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class RenameButtonHandle implements MouseListener {
    private FilesListHandle filesListHandle;

    public RenameButtonHandle(FilesListHandle filesListHandle) {
        this.filesListHandle=filesListHandle;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        filesListHandle.renameFile();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
