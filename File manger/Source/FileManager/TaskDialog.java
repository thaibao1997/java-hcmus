package FileManager;

import javax.swing.*;
import java.awt.event.*;

public class TaskDialog extends JDialog {
    private JProgressBar progressBar;
    private JLabel label;
    private boolean canClose=false;
    TaskDialog(String title){
        super(null,title,ModalityType.MODELESS);
        setLayout(null);
        setResizable(false);
        setSize(405,150);

        label=new JLabel();
        label.setLocation(10,10);
        label.setSize(380,50);

        progressBar=new JProgressBar();
        progressBar.setLocation(label.getX(),label.getY()+label.getHeight());
        progressBar.setSize(380,40);

        add(label);
        add(progressBar);

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

    }

    public void setLabel(String text){
        SwingUtilities.invokeLater(
                new Runnable() {
                    @Override
                    public void run() {
                        label.setText(text);
                    }
                }
        );
    }

    public void setMaximun(int maximun){
        SwingUtilities.invokeLater(
                new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setMaximum(maximun);
                    }
                }
        );
    }

    public void setValue(int value){
        SwingUtilities.invokeLater(
                new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setValue(value);
                    }
                }
        );
    }

    public void setCanClose(boolean canClose) {
        this.canClose = canClose;
    }

    public int getValue(){
        return progressBar.getValue();
    }


    public JProgressBar getProgressBar() {
        return progressBar;
    }

    public JLabel getLabel() {
        return label;
    }


}
