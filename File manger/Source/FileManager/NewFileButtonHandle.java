package FileManager;

import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class NewFileButtonHandle implements MouseListener {
    private FilesListHandle filesListHandle;

    public NewFileButtonHandle(FilesListHandle filesListHandle) {
        this.filesListHandle=filesListHandle;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        filesListHandle.createNewFile();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
