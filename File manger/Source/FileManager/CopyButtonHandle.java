package FileManager;


import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CopyButtonHandle implements MouseListener {
    private FilesListHandle filesListHandle;
    private JButton pasteButton;

    public CopyButtonHandle(FilesListHandle filesListHandle,JButton pasteButton) {
        this.filesListHandle=filesListHandle;
        this.pasteButton=pasteButton;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if(filesListHandle.getCopyFiles())
            pasteButton.setEnabled(true);

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
