package FileManager;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.table.*;
import javax.swing.tree.*;

import java.awt.event.*;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import javax.swing.filechooser.*;
import java.text.*;

public class FilesTreeHandle implements MouseListener{
    private FilesListHandle filesListHandle;
    private FileSystemView fsv = FileSystemView.getFileSystemView();

    public FilesTreeHandle(FilesListHandle filesListHandle){
        this.filesListHandle=filesListHandle;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        JTree tree =(JTree) mouseEvent.getSource();
        DefaultMutableTreeNode SelectedNode= (DefaultMutableTreeNode)tree.getLastSelectedPathComponent();
        if(SelectedNode != null) {
            if(SelectedNode== tree.getModel().getRoot()){
                filesListHandle.loadDrivesList(false);
            }else {
                //String path=getPathFromTreeNode(SelectedNode);
                File file = (File) (SelectedNode.getUserObject());
                filesListHandle.loadFilesList(file.getPath(),false);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    public String getPathFromTreeNode(DefaultMutableTreeNode node){
        TreeNode[] nodes = node.getPath();
        String path = "";
        for (int i = 1; i < nodes.length - 1; i++) {
            if (i != 1)
                path += nodes[i].toString() + "\\";
            else
                path += nodes[i].toString();
        }
        path += nodes[nodes.length-1].toString();
        System.out.println(path);


        return path;
    }

    public boolean hasSubFolder(String path){
        File file=new File(path);
        if(file.isDirectory()){
            File[] children=file.listFiles();
            if(children==null)
                return false;
            for (File child:children) {
                if(child.isDirectory())
                    return true;
            }
            return false;
        }else{
            return false;
        }
    }


    public  TreeWillExpandListener getTreeWillExpandListener(){
        return this.treeWillExpandListener;
    }

    private TreeWillExpandListener treeWillExpandListener = new TreeWillExpandListener() {
        public void treeWillCollapse(TreeExpansionEvent treeExpansionEvent)
                throws ExpandVetoException {
            TreePath path = treeExpansionEvent.getPath();
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
            //node.removeAllChildren();
        }

        public void treeWillExpand(TreeExpansionEvent treeExpansionEvent) throws ExpandVetoException {

            TreePath path = treeExpansionEvent.getPath();
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
            node.removeAllChildren();

            //String path_=getPathFromTreeNode(node);

            File file=(File)node.getUserObject();
            File[] files=file.listFiles();
            if(files != null) {
                for (File file_ : files) {
                    if (file_.isDirectory() && !file_.isHidden()) {
                        DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(file_);
                        node.add(newNode);
                        if (hasSubFolder(file_.getPath())) {
                            DefaultMutableTreeNode dummy = new DefaultMutableTreeNode("dummy");
                            newNode.add(dummy);
                        }
                    }
                }
            }

        }
    };



}
