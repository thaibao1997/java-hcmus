package FileManager;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;


public class NewFolderButtonHandle implements MouseListener {
    private FilesListHandle filesListHandle;

    public NewFolderButtonHandle(FilesListHandle filesListHandle) {
        this.filesListHandle=filesListHandle;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        filesListHandle.createNewFolder();
        //filesListHandle.unzipFile();


    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
