package FileManager;

import javax.swing.table.DefaultTableModel;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.zip.ZipInputStream;

public class Untils {

    static public boolean copyDirectory(File src, File dest) throws IOException {
        if (!src.exists()) {
            return false;
        }
        if (src.isDirectory()) {
            if (!dest.exists()) {
                dest.mkdir();
            }
            File[] files = src.listFiles();
            for (File file : files) {
                File sourceFile = new File(src, file.getName());
                File outputFile = new File(dest, file.getName());
                if (file.isDirectory()) {
                    copyDirectory(sourceFile, outputFile);
                } else {
                    Files.copy(sourceFile.toPath(), outputFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
            }
        }
        return true;
    }

    static public void copyFile(File source,File dest){
        if(!source.exists())
            return;
        try {
            if(!dest.exists())
                dest.createNewFile();
            FileInputStream fIS= new FileInputStream(source);
            FileOutputStream fOS=new FileOutputStream(dest);
            BufferedInputStream inFile=new BufferedInputStream(fIS);
            BufferedOutputStream outFile=new BufferedOutputStream(fOS);
            int readByte;
            long fileSize=source.length();
            for(int i=0;i<fileSize;i++){
                readByte = inFile.read();
                if(readByte != -1)
                    outFile.write(readByte);
            }
            inFile.close();
            outFile.close();
            fIS.close();
            fOS.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static public boolean isZippedFile(File file){
        boolean isZipped=false;
        try {
            isZipped = new ZipInputStream(new FileInputStream(file)).getNextEntry() != null;
        }catch (Exception ex){
            ex.getStackTrace();
        }
        return isZipped;
    }

    static public boolean isSplittedFirstFile(File file){
        String name= file.getName();
        if(!name.matches("(.+)(1$)")){
            return false;
        }
        return true;
    }

    static public ArrayList<File> getAllFiles(File parent){
        ArrayList<File> res=new ArrayList<>();
        if(parent.isFile()) {
            res.add(parent);
            return res;
        }
        File[] children=parent.listFiles();
        if(children==null || children.length < 1)
            return res;
        for(File child : children){
            if(child.isFile())
                res.add(child);
            else
                res.addAll(getAllFiles(child));
        }
        return res;
    }

    static public void removeAllTableRow(DefaultTableModel table){
        int rowCount = table.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            table.removeRow(i);
        }
    }

    static public String  getLastModifiedDate(File file){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return sdf.format(file.lastModified());
    }


    static public String converFileSize(long size){
        float size_=size;
        String[] metrics={" Bytes"," KB"," MB"," GB"};
        int count=0;
        while (size_ / 1024.0 >= 1 && count < metrics.length){
            count++;
            size_ /= 1024.0;
        }
        String result=String.format("%.2f %s",size_,metrics[count]);
        return result;
    }
}
