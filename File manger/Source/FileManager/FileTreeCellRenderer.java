package FileManager;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;
import java.io.File;

public class FileTreeCellRenderer implements TreeCellRenderer {
    private FileSystemView fsv=FileSystemView.getFileSystemView();
    private DefaultTreeCellRenderer defaultRenderer = new DefaultTreeCellRenderer();
    private Font font=new Font(Font.SANS_SERIF,Font.PLAIN,14);

    @Override
    public Component getTreeCellRendererComponent(JTree jTree, Object o, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)o;
        File nodeObject = (File) node.getUserObject();
        JLabel label=new JLabel();
        label.setIcon(fsv.getSystemIcon(nodeObject));
        label.setText(fsv.getSystemDisplayName(nodeObject));
        label.setFont(font);
        if(selected) {
            label.setForeground(Color.BLUE);
        }
        return label;
        //return defaultRenderer.getTreeCellRendererComponent(jTree,label,b,b1,b2,i,b3);
    }
}
