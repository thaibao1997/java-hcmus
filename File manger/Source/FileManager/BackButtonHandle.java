package FileManager;

import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class BackButtonHandle implements MouseListener {
    private FilesListHandle filesListHandle;

    public BackButtonHandle(FilesListHandle filesListHandle){
        this.filesListHandle=filesListHandle;
    }


    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        filesListHandle.goBack();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
