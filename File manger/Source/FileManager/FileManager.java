package FileManager;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.io.File;

public class FileManager extends JFrame {
    private JTree tree;
    private JTable table;
    private DefaultTreeModel treeModel;
    private DefaultTableModel tableModel;

    private JTextField pathText;
    private JButton backButton;
    private JButton goButton;

    private JButton openButton;
    private JButton newFolderButton;
    private JButton newFileButton;
    private JButton pasteButton;
    private JButton copyButton;
    private JButton renameButton;
    private JButton zipButton;
    private JButton unzipButton;
    private JButton splitFileButton;
    private JButton mergeFileButton;

    private JLabel taskLabel;
    private JProgressBar progressBar;

    private FilesListHandle filesListHandle;
    private FilesTreeHandle filesTreeHandle;

    private static final int window_height = 700;
    private static final int window_width = 900;
    private static final int button_size=55;
    private static final int button_offset = 3;
    private static final int dir_height = 25;
    private static final int dir_button_lenght = 35;
    private static final int side_offset = 10;

    public FileManager() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        super("File Manager");
        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");

        setSize(window_width,window_height);
        setLayout(null);
        setResizable(false);
        SwingUtilities.updateComponentTreeUI(this);

        //setup Tree
        DefaultMutableTreeNode rootNode=new DefaultMutableTreeNode(new File("This PC"));
        treeModel=new DefaultTreeModel(rootNode);
        tree=new JTree(treeModel);
        tree.setBackground(Color.white);
        tree.setLocation(0,0);
        tree.setSize(230,515);
        tree.setRowHeight(20);

        //setup table
        tableModel=new DefaultTableModel();
        tableModel.addColumn(" ");
        tableModel.addColumn("Name");
        tableModel.addColumn("Type");
        tableModel.addColumn("Size");
        tableModel.addColumn("Last Modified");
        tableModel.addColumn("Path");

        table=new JTable(tableModel);
        table.setAutoCreateRowSorter(false);
        table.setShowGrid(false);
        table.setDefaultEditor(Object.class, null);
        table.setRowHeight(50);
        table.getColumnModel().getColumn(0).setCellRenderer(new IconRenderer());
        table.getColumnModel().getColumn(0).setMaxWidth(25);
        table.removeColumn(table.getColumnModel().getColumn(5));


        //to show scroll bar
        JScrollPane treeScrP=new JScrollPane(tree);
        treeScrP.setLocation(side_offset,button_size + dir_height + 2*side_offset -button_offset);
        treeScrP.setSize(window_width/4,window_height- treeScrP.getLocation().y - 5*side_offset);
        treeScrP.getViewport().setBackground(Color.WHITE);
        treeScrP.setBorder(BorderFactory.createLineBorder(Color.BLACK));


        JScrollPane tableSrP=new JScrollPane(table);
        tableSrP.setLocation(treeScrP.getLocation().x+treeScrP.getSize().width,treeScrP.getLocation().y);
        tableSrP.setSize(window_width-(treeScrP.getLocation().x + treeScrP.getSize().width+3*side_offset),treeScrP.getSize().height);
        tableSrP.getViewport().setBackground(Color.WHITE);
        tableSrP.setBorder(BorderFactory.createLineBorder(Color.BLACK));



        //setup show path
        backButton=new JButton();
        backButton.setLocation(side_offset,treeScrP.getLocation().y-(dir_height+2));
        backButton.setSize(dir_button_lenght-5,dir_height);
        backButton.setBackground(Color.WHITE);
        backButton.setEnabled(false);
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/back.png"));
            img = img.getScaledInstance(18, 18, Image.SCALE_SMOOTH);
            backButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        pathText=new JTextField("This PC");
        pathText.setLocation(backButton.getLocation().x+backButton.getSize().width,backButton.getLocation().y);
        pathText.setSize(window_width-(pathText.getLocation().x+dir_button_lenght+3*side_offset),dir_height);

        goButton=new JButton();
        goButton.setLocation(pathText.getLocation().x+pathText.getSize().width,pathText.getLocation().y);
        goButton.setSize(dir_button_lenght,dir_height);
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/go.png"));
            goButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //setup buttons

        openButton=new JButton();
        openButton.setLocation(side_offset,side_offset);
        openButton.setSize(button_size,button_size);
        openButton.setToolTipText("Open...");
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/open.png"));
            openButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        newFolderButton = new JButton();
        newFolderButton.setLocation(openButton.getLocation().x+openButton.getSize().width+button_offset,openButton.getLocation().y);
        newFolderButton.setSize(button_size,button_size);
        newFolderButton.setToolTipText("Create New Folder Here");
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/add_folder.png"));
            newFolderButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        newFileButton =new JButton();
        newFileButton.setLocation(newFolderButton.getLocation().x+newFolderButton.getSize().width+button_offset,newFolderButton.getLocation().y);
        newFileButton.setSize(button_size,button_size);
        newFileButton.setToolTipText("Create New Text File(.txt) Here");
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/add_file.png"));
            newFileButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        copyButton=new JButton();
        copyButton.setLocation(newFileButton.getLocation().x+newFileButton.getSize().width+button_offset,newFileButton.getLocation().y);
        copyButton.setSize(button_size,button_size);
        copyButton.setToolTipText("Copy File(s)");
        copyButton.setEnabled(false);
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/copy.png"));
            copyButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        pasteButton=new JButton();
        pasteButton.setLocation(copyButton.getLocation().x+copyButton.getSize().width+button_offset,copyButton.getLocation().y);
        pasteButton.setSize(button_size,button_size);
        pasteButton.setEnabled(false);
        pasteButton.setToolTipText("Paste Files(s) Here");
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/paste.png"));
            pasteButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        renameButton=new JButton();
        renameButton.setLocation(pasteButton.getLocation().x+pasteButton.getSize().width+button_offset,pasteButton.getLocation().y);
        renameButton.setSize(button_size,button_size);
        renameButton.setToolTipText("Rename File or Folder");
        renameButton.setEnabled(false);
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/rename.png"));
            renameButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        zipButton=new JButton();
        zipButton.setLocation(renameButton.getLocation().x+renameButton.getSize().width+button_offset,renameButton.getLocation().y);
        zipButton.setSize(button_size,button_size);
        zipButton.setToolTipText("Compress File To .Zip File In Current Folder");
        zipButton.setEnabled(false);
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/zip.png"));
            zipButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        unzipButton=new JButton();
        unzipButton.setLocation(zipButton.getLocation().x+zipButton.getSize().width+button_offset,zipButton.getLocation().y);
        unzipButton.setSize(button_size,button_size);
        unzipButton.setToolTipText("Extract .Zip File To Current Folder");
        unzipButton.setEnabled(false);
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/unzip.png"));
            unzipButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        splitFileButton=new JButton();
        splitFileButton.setLocation(unzipButton.getLocation().x+unzipButton.getSize().width+button_offset,unzipButton.getLocation().y);
        splitFileButton.setSize(button_size,button_size);
        splitFileButton.setToolTipText("Split File Into Smaller Files");
        splitFileButton.setEnabled(false);
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/split.png"));
            splitFileButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        mergeFileButton=new JButton();
        mergeFileButton.setLocation(splitFileButton.getLocation().x+splitFileButton.getSize().width+button_offset,splitFileButton.getLocation().y);
        mergeFileButton.setSize(button_size,button_size);
        mergeFileButton.setToolTipText("Merge Splitted Files Into One File");
        mergeFileButton.setEnabled(false);
        try {
            Image img = ImageIO.read(getClass().getResource("Icons/merge.png"));
            mergeFileButton.setIcon(new ImageIcon(img));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        taskLabel=new JLabel();
        /*progressBar=new JProgressBar();
        progressBar.setLocation(tableSrP.getX(),tableSrP.getY()+tableSrP.getHeight() + 2);
        progressBar.setSize(tableSrP.getWidth(),20);
        add(progressBar);*/

        add(tableSrP);
        add(treeScrP);
        add(pathText);
        add(goButton);
        add(backButton);
        add(openButton);
        add(newFolderButton);
        add(newFileButton);
        add(copyButton);
        add(pasteButton);
        add(renameButton);
        add(zipButton);
        add(unzipButton);
        add(splitFileButton);
        add(mergeFileButton);

        filesListHandle=new FilesListHandle(this);
        filesTreeHandle=new FilesTreeHandle(filesListHandle);
        startUpLoad();


        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void startUpLoad(){
        File[] paths;
        FileSystemView fsv = FileSystemView.getFileSystemView();
        paths = File.listRoots();
        DefaultMutableTreeNode root=(DefaultMutableTreeNode)treeModel.getRoot();

        for(File path:paths)
        {
            DefaultMutableTreeNode node=new DefaultMutableTreeNode(path);
            if(filesTreeHandle.hasSubFolder(path.getPath())) {
                DefaultMutableTreeNode dummy = new DefaultMutableTreeNode("dummy");
                node.add(dummy);
            }
            root.add(node);
        }

        tree.expandPath(new TreePath(root.getPath()));

        table.addMouseListener(filesListHandle);
        ListSelectionModel selectionModel = table.getSelectionModel();
        selectionModel.addListSelectionListener(filesListHandle);

        filesListHandle.loadDrivesList(false);

        tree.addMouseListener(filesTreeHandle);
        tree.addTreeWillExpandListener(filesTreeHandle.getTreeWillExpandListener());
        tree.setCellRenderer(new FileTreeCellRenderer());

        backButton.addMouseListener(new BackButtonHandle(filesListHandle));
        goButton.addMouseListener(new GoButtonHandle(filesListHandle));
        newFolderButton.addMouseListener(new NewFolderButtonHandle(filesListHandle));
        newFileButton.addMouseListener(new NewFileButtonHandle(filesListHandle));
        copyButton.addMouseListener(new CopyButtonHandle(filesListHandle,pasteButton));
        pasteButton.addMouseListener(new PasteButtonHandle(filesListHandle));
        renameButton.addMouseListener(new RenameButtonHandle(filesListHandle));
        zipButton.addMouseListener(new ZipButtonHandle(filesListHandle));
        unzipButton.addMouseListener(new UnzipButtonHandle(filesListHandle));
        splitFileButton.addMouseListener(new SplitButtonHandle(filesListHandle));
        mergeFileButton.addMouseListener(new MergeButtonHandle(filesListHandle));
        openButton.addMouseListener(new OpenButtonHandle(filesListHandle));


    }

    public JButton getBackButton() {
        return backButton;
    }

    public JButton getGoButton() {
        return goButton;
    }

    public JButton getOpenButton() {
        return openButton;
    }

    public JButton getNewFolderButton() {
        return newFolderButton;
    }

    public JButton getNewFileButton() {
        return newFileButton;
    }

    public JButton getPasteButton() {
        return pasteButton;
    }

    public JButton getCopyButton() {
        return copyButton;
    }

    public JButton getRenameButton() {
        return renameButton;
    }

    public JButton getZipButton() {
        return zipButton;
    }

    public JButton getUnzipButton() {
        return unzipButton;
    }

    public JButton getSplitFileButton() {
        return splitFileButton;
    }

    public JButton getMergeFileButton() {
        return mergeFileButton;
    }

    public JTable getTable() {
        return table;
    }

    public JTextField getPathText() {
        return pathText;
    }

}