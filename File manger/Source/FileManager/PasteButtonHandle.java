package FileManager;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class PasteButtonHandle implements MouseListener {

 private FilesListHandle filesListHandle;

    public PasteButtonHandle(FilesListHandle filesListHandle) {
        this.filesListHandle=filesListHandle;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
               filesListHandle.pasteFiles();
               return null;
            }
        };

        worker.execute();
        JButton pasteButton= (JButton)mouseEvent.getSource();
        pasteButton.setEnabled(false);
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}