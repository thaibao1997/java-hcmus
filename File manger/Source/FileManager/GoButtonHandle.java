package FileManager;


import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GoButtonHandle implements MouseListener {
    private FilesListHandle filesListHandle;

    public GoButtonHandle(FilesListHandle filesListHandle){
        this.filesListHandle=filesListHandle;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        filesListHandle.goToPath();
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
