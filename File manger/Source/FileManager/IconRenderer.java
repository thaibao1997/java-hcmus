package FileManager;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class IconRenderer extends DefaultTableCellRenderer {
    public IconRenderer() {
        super();
    }

    public void setValue(Object value) {
        if (value == null) {
            setText("");
        } else {
            setIcon((Icon) value);
        }
    }
}
