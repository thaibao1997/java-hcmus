package FileManager;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import javax.swing.filechooser.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.text.*;
import java.util.Stack;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;
import java.util.zip.*;

import static java.lang.Thread.sleep;

public class FilesListHandle implements MouseListener,ListSelectionListener {
    private FileSystemView fsv = FileSystemView.getFileSystemView();
    FileManager fileManager;

    private Stack<File> previousDir;
    private File currentDir=null;
    private ArrayList<File> copyFiles;

    public FilesListHandle(FileManager fileManager){
        previousDir=new Stack<>();
        copyFiles=new ArrayList<>();
        this.fileManager=fileManager;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

        JTable table =(JTable) mouseEvent.getSource();
        DefaultTableModel tableModel=(DefaultTableModel)table.getModel();
        if(SwingUtilities.isLeftMouseButton(mouseEvent)){
            int row = table.getSelectedRow();
            String path = ((File) tableModel.getValueAt(row, 5)).getAbsolutePath();
            if (mouseEvent.getClickCount() == 2) {
                loadFilesList(path, false);
            }
        }else if(SwingUtilities.isRightMouseButton(mouseEvent)){
            int row=table.rowAtPoint(mouseEvent.getPoint());
            if (row >= 0 && row < table.getRowCount()) {
                table.setRowSelectionInterval(row, row);
            } else {
                table.clearSelection();
            }
        }
    }

    public void mousePressed(MouseEvent mouseEvent)  {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    public void goBack(){
        if(!previousDir.isEmpty()){
            File dir=previousDir.pop();
            if(dir ==  null) return;
            if(dir.getPath().equals("This PC"))
                loadDrivesList(true);
            else
                loadFilesList(dir.getAbsolutePath(),true);
            if(previousDir.isEmpty()) {
                JButton backButton=fileManager.getBackButton();
                backButton.setEnabled(false);
            }
        }
    }

    public void goToPath(){
        JTextField pathField = fileManager.getPathText();
        String path=pathField.getText();
        if(path.compareTo(currentDir.getAbsolutePath())!=0){
            File file=new File(path);
            if(file.exists()){
                loadFilesList(path,false);
                if(file.isFile()){
                    pathField.setText(currentDir.getAbsolutePath());
                }
            }else{
                JOptionPane.showMessageDialog(null, "File Not Found!!!");
                pathField.setText(currentDir.getAbsolutePath());
            }
        }
    }

    public void loadFilesList(String path,boolean isGoBackCall){
        JTable table=fileManager.getTable();
        JTextField pathField = fileManager.getPathText();
        File file=new File(path);
        DefaultTableModel tableModel=(DefaultTableModel)table.getModel();

        if(file.isDirectory()) {
            Untils.removeAllTableRow(tableModel);
            File[] children = file.listFiles();
            if(children==null) return;
            ArrayList<File> folderLists=new ArrayList<>();
            ArrayList<File> filesList=new ArrayList<>();

            pathField.setText(file.getPath());
            System.out.println("Open Folder: "+file.getPath());
            for (File child : children) {
                if(child.isHidden()) continue;
                if(child.isDirectory()) {
                    folderLists.add(child);
                }else{
                    filesList.add(child);
                }
            }
            for(File folder: folderLists){
                tableModel.addRow(new Object[]{fsv.getSystemIcon(folder), fsv.getSystemDisplayName(folder),
                        fsv.getSystemTypeDescription(folder), "", Untils.getLastModifiedDate(file)
                        , folder});
            }

            for(File file_: filesList){
                tableModel.addRow(new Object[]{fsv.getSystemIcon(file_), fsv.getSystemDisplayName(file_),
                        fsv.getSystemTypeDescription(file_), Untils.converFileSize(file_.length()), Untils.getLastModifiedDate(file_)
                        , file_});
            }
            if(!isGoBackCall) {
                previousDir.push(currentDir);
                JButton backButton=fileManager.getBackButton();
                backButton.setEnabled(true);
            }
            currentDir=new File(path);

        }else{//run file
            System.out.println("Run: "+file.getAbsolutePath());
            try {
                Process process = new ProcessBuilder("cmd","/C",file.getAbsolutePath()).start();
                process.waitFor(200,TimeUnit.MILLISECONDS);
                process.destroy();
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }

    public void openFile(){
        JTable table=fileManager.getTable();
        int row=table.getSelectedRow();
        if(row != -1) {
            File selectFile = (File)table.getModel().getValueAt(row,5);
            if(selectFile != null){
                loadFilesList(selectFile.getAbsolutePath(),false);
            }
        }
    }

    public void loadDrivesList(boolean isGoBackCall){
        if(!isGoBackCall) {
            previousDir.push(currentDir);
                JButton backButton=fileManager.getBackButton();
                backButton.setEnabled(true);
        }
        JTable table=fileManager.getTable();
        JTextField pathField = fileManager.getPathText();
        currentDir=new File("This PC");
        File[] paths;
        fsv = FileSystemView.getFileSystemView();
        paths = File.listRoots();
        DefaultTableModel tableModel=(DefaultTableModel)table.getModel();
        Untils.removeAllTableRow(tableModel);
        pathField.setText("This PC");
        for(File path:paths)
        {
            tableModel.addRow(new Object[]{fsv.getSystemIcon(path),fsv.getSystemDisplayName(path),
                    fsv.getSystemTypeDescription(path),Untils.converFileSize(path.getTotalSpace()),"",path});

        }
    }

    public boolean createNewFolder(){
        if(currentDir.getPath().equals("This PC")){
            JOptionPane.showMessageDialog(null,"Can not create new folder here");
            return false;
        }

        String name= JOptionPane.showInputDialog("Name Of New Folder: ","New Folder");
        if(name == null || name.isEmpty())
            return false;

        File file=new File(currentDir,name);
        if(file.exists()){
            JOptionPane.showMessageDialog(null,"folder "+name+" existed");
            return false;
        }
        boolean res= file.mkdir();
        if(res)
            loadFilesList(currentDir.getAbsolutePath(),true);
        return res;
    }

    public boolean createNewFile(){
        if(currentDir.getPath().equals("This PC")){
            JOptionPane.showMessageDialog(null,"Can not create new file here");
            return false;
        }
        String name= JOptionPane.showInputDialog("Name Of New File: ","New Text Document");
        if(name == null || name.isEmpty())
            return false;

        File file=new File(currentDir + "/" + name+".txt");
        if(file.exists()){
            String message=String.format("%s\n%s","file " +name+".txt existed","Do you want to override it?");
            int override= JOptionPane.showConfirmDialog(null,message,"Confirm",JOptionPane.YES_NO_OPTION);
            if(override == JOptionPane.NO_OPTION){
                return false;
            }
        }
        boolean res= false;
        try {
            file.delete();
            res = file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(res)
            loadFilesList(currentDir.getAbsolutePath(),true);
        return res;
    }

    public void renameFile(){
        if(currentDir.getPath().equals("This PC")){
            JOptionPane.showMessageDialog(null,"Can not rename drives");
            return;
        }

        JTable table=fileManager.getTable();
        DefaultTableModel tableModel=(DefaultTableModel)table.getModel();

        int row=table.getSelectedRow();
        if(row == -1) {
            JOptionPane.showMessageDialog(null, "Nothing to rename");
            return;
        }
        File currentFile=(File)tableModel.getValueAt(row,5);
        String newName=JOptionPane.showInputDialog("New Name Of File: ",currentFile.getName());
        if(newName == null ||  newName.isEmpty()){
            return;
        }

        File newFile=new File(currentFile.getParent(),newName);
        if(newFile.exists()) {
            JOptionPane.showMessageDialog(null, newFile + " existed");
            return;
        }
        try {
            System.out.println(currentFile);
            System.out.println(newFile);
            if(currentFile.isFile()) {
                System.gc();
                Untils.copyFile(currentFile,newFile);
                Files.deleteIfExists(currentFile.toPath());
            }else {
                boolean res= currentFile.renameTo(newFile);
                System.out.println(res);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.loadFilesList(currentDir.getAbsolutePath(),true);
    }

    public boolean getCopyFiles(){
        if(currentDir.getPath().equals("This PC")){
            JOptionPane.showMessageDialog(null,"Can not copy file(s)");
            return false;
        }
        JTable table=fileManager.getTable();
        int[] selection =table.getSelectedRows();
        DefaultTableModel tableModel=(DefaultTableModel)table.getModel();

        if(selection==null || selection.length < 1){
            JOptionPane.showMessageDialog(null,"Nothing To Copy");
            return false;
        }
        if(!copyFiles.isEmpty())
            copyFiles.clear();
        for(int row : selection){
            copyFiles.add((File)tableModel.getValueAt(row,5));
        }

        if(copyFiles != null && !copyFiles.isEmpty())
            return true;
        return false;
    }

    public void pasteFiles(){
        if(currentDir.getPath().equals("This PC")){
            JOptionPane.showMessageDialog(null,"Can not create paste file(s) here");
            return;
        }
        JTable table=fileManager.getTable();
        DefaultTableModel tableModel=(DefaultTableModel)table.getModel();

        if(copyFiles==null || copyFiles.isEmpty()){
            JOptionPane.showMessageDialog(null,"Nothing To Paste");
            return;
        }

        File openFolder=currentDir;
        TaskDialog dlg=new TaskDialog("Copy");
        dlg.setLabel("Copying File(s)...");
        dlg.setMaximun(copyFiles.size());
        dlg.setVisible(true);
        int count=0;
        dlg.setValue(count);

        for (File file:copyFiles){
            dlg.setLabel("<html><body>Copying File:<br>" + file.getAbsolutePath()+"</body></html>");
            if(file.exists()) {
                File dest=new File(currentDir ,file.getName());
                System.gc();
                try {
                    if(file.isFile()) {
                        Untils.copyFile(file,dest);
                    }
                    else
                        Untils.copyDirectory(file, dest);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            dlg.setValue(++count);
        }
        copyFiles.clear();
        System.out.println("dlg val: "+dlg.getValue());
        dlg.setLabel("<html><body>Completed Copying File(s) to:<br>" + openFolder.getAbsolutePath()+"</body></html>");
        dlg.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        if(currentDir.getAbsolutePath().equals(openFolder.getAbsolutePath()))
            loadFilesList(currentDir.getAbsolutePath(),true);

    }

    public void compressFile(){
        if(currentDir.getPath().equals("This PC")){
            JOptionPane.showMessageDialog(null,"Can Compress File Here");
            return;
        }
        JTable table=fileManager.getTable();
        int[] selection =table.getSelectedRows();

        if(selection==null || selection.length < 1){
            JOptionPane.showMessageDialog(null,"Nothing To Compress");
            return;
        }
        String nameZip=JOptionPane.showInputDialog(null,"Name Zip File:");
        if(nameZip==null || nameZip.isEmpty())
            return;

        DefaultTableModel tableModel=(DefaultTableModel)table.getModel();

        ArrayList<File> compressFiles=new ArrayList<>();
        for(int row : selection){
            File file=(File)tableModel.getValueAt(row,5);
            compressFiles.addAll(Untils.getAllFiles(file));
        }


        TaskDialog dlg=new TaskDialog("Compress File");
        dlg.setLabel("Compressing File(s)...");
        dlg.setMaximun(compressFiles.size());
        dlg.setVisible(true);
        int count=0;
        dlg.setValue(count);

        File openFolder=currentDir;
        FileOutputStream fOS=null;
        ZipOutputStream zipOS=null;
        File outFile=new File(currentDir,nameZip+".zip");
        try {
            fOS=new FileOutputStream(outFile);
            zipOS=new ZipOutputStream(fOS);
            char lastChar=currentDir.getPath().charAt(currentDir.getPath().length()-1);
            String currentPath="";
            if( lastChar != '\\' && lastChar !='/')
                 currentPath=currentDir+"/";
            byte[] buffer=new byte[1024];
            for (File file:compressFiles){
                if(file.exists()) {
                    dlg.setLabel("<html><body>Compressing File:<br>" + file.getAbsolutePath()+"</body></html>");
                    String aPath = file.getPath();
                    String rPath=aPath.substring(currentPath.length());
                    ZipEntry zipEntry=new ZipEntry(rPath);
                    zipOS.putNextEntry(zipEntry);
                    int len;
                    FileInputStream fileIs = new FileInputStream(file);
                    while ((len=fileIs.read(buffer)) > 0){
                        zipOS.write(buffer,0,len);
                    }
                    dlg.setValue(++count);
                }
            }

        }catch (Exception ex){
            System.out.println(ex);
        }finally {
            try {
                zipOS.close();
                fOS.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            dlg.setLabel("<html><body>Completed Compress File To:<br>" + outFile.getAbsolutePath()+"</body></html>");
            dlg.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            if(currentDir.getAbsolutePath().equals(openFolder.getAbsolutePath()))
                loadFilesList(currentDir.getAbsolutePath(),true);
        }

    }

    public void unzipFile(){
        if(currentDir.getPath().equals("This PC")){
            JOptionPane.showMessageDialog(null,"Can Unzip File Here");
            return;
        }
        JTable table=fileManager.getTable();
        int selection =table.getSelectedRow();
        if(selection==-1){
            JOptionPane.showMessageDialog(null,"Nothing To UnZip");
            return;
        }
        File zipFile=(File)table.getModel().getValueAt(selection,5);
        if(!Untils.isZippedFile(zipFile)){
            JOptionPane.showMessageDialog(null,"Not Zipped File");
            return;
        }
        ZipInputStream zipIS=null;

        TaskDialog dlg=new TaskDialog("UnZip File");
        dlg.setMaximun((int)zipFile.length());
        dlg.setVisible(true);
        int count=0;
        dlg.setValue(count);
        dlg.setLabel("<html><body>Unzipping File:<br>"+zipFile.getAbsolutePath()+"</body></html>");
        File openFolder=currentDir;

        try {
            zipIS=new ZipInputStream(new FileInputStream(zipFile));
            ZipEntry zipEntry=null;
            byte[] buffer=new byte[1024];
            while ((zipEntry = zipIS.getNextEntry()) != null){
                String entryName=zipEntry.getName();
                File outFile= new File(currentDir,entryName);
                if(zipEntry.isDirectory()) {
                    outFile.mkdir();
                }else {
                    outFile.getParentFile().mkdirs();
                    FileOutputStream outputStream=new FileOutputStream(outFile);
                    int len;
                    while ((len = zipIS.read(buffer)) > 0) {
                        outputStream.write(buffer, 0, len);
                        count+=len;
                        dlg.setValue(count);
                    }
                    outputStream.close();
                }
            }
        }catch (Exception ex) {
            System.out.println(ex);
        }
        System.out.println("COunt: "+count);
        dlg.setLabel("<html><body>Completed Unzip File To:<br>" + openFolder.getAbsolutePath()+"</body></html>");
        dlg.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        if(currentDir.getAbsolutePath().equals(openFolder.getAbsolutePath()))
            loadFilesList(currentDir.getAbsolutePath(),true);

    }

    public void spitFile(){
        if(currentDir.getPath().equals("This PC")){
            JOptionPane.showMessageDialog(null,"Can Split File Here");
            return;
        }
        JTable table=fileManager.getTable();
        int selection =table.getSelectedRow();

        if(selection==-1){
            JOptionPane.showMessageDialog(null,"Nothing To Split");
            return;
        }

        File file=(File) table.getModel().getValueAt(selection,5);
        if(file.isDirectory()){
            JOptionPane.showMessageDialog(null,"Can not split folder");
            return;
        }
        int numberOfSplits=0;
        boolean error=false;
        do {
            error=false;
            String input = JOptionPane.showInputDialog("Enter Number Of Parts ");
            if(input==null)
                return;
            if (input.matches("[0-9]+")) {
                numberOfSplits=Integer.parseInt(input);
                if(numberOfSplits<2)
                    error=true;
            }else{
                error=true;
            }
            if(error){
                JOptionPane.showMessageDialog(null,"Please enter a number greater than 2");
            }
        } while (error);

        long fileSize=file.length();

        if(fileSize < numberOfSplits){
            JOptionPane.showMessageDialog(null,"Can not split file");
            return;
        }

        TaskDialog dlg=new TaskDialog("Split File");
        dlg.setMaximun(numberOfSplits);
        dlg.setVisible(true);
        int count=0;
        dlg.setValue(count);
        dlg.setLabel("<html><body>Splitting File:<br>"+file.getAbsolutePath()+"</body></html>");
        File openFolder=currentDir;


        long sizePerParts=fileSize/numberOfSplits;
        long lastPartFile=sizePerParts + fileSize % numberOfSplits;

        try {
            FileInputStream fIS=new FileInputStream(file);
            BufferedInputStream bIS = new BufferedInputStream(fIS);
            for (int i=1;i<numberOfSplits;i++){
                String namePart=file.getParent()+"/"+file.getName()+"."+i;
                BufferedOutputStream bOS=new BufferedOutputStream(new FileOutputStream(namePart));
                for (int j=0;j<sizePerParts;j++){
                    int readByte=bIS.read();
                    if(readByte != -1) {
                        bOS.write(readByte);
                    }else {
                        return;
                    }
                }
                dlg.setValue(++count);
                bOS.close();
            }

            String namePart=file.getParent()+"/"+file.getName()+"."+numberOfSplits;
            BufferedOutputStream bOS=new BufferedOutputStream(new FileOutputStream(namePart));
            for (int j=0;j<lastPartFile;j++){
                int readByte=bIS.read();
                if(readByte != -1) {
                    bOS.write(readByte);
                }else {
                    return;
                }
            }
            dlg.setValue(++count);
            bOS.close();
            bIS.close();
            fIS.close();
        }catch (Exception ex){
            System.out.println(ex);
        }
        dlg.setLabel("<html><body>Completed Split File To:<br>" + openFolder.getAbsolutePath()+"</body></html>");
        dlg.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        if(currentDir.getAbsolutePath().equals(openFolder.getAbsolutePath()))
            loadFilesList(currentDir.getAbsolutePath(),true);
    }

    public void mergeFiles() {
        if(currentDir.getPath().equals("This PC")){
            JOptionPane.showMessageDialog(null,"Can Merge Files Here");
            return;
        }
        JTable table=fileManager.getTable();
        int selection =table.getSelectedRow();

        if(selection==-1){
            JOptionPane.showMessageDialog(null,"Nothing To Merge");
            return;
        }

        File file=(File) table.getModel().getValueAt(selection,5);
        if(file.isDirectory()) {
            JOptionPane.showMessageDialog(null, "Can not merge");
            return;
        }

        if(!Untils.isSplittedFirstFile(file)){
            JOptionPane.showMessageDialog(null,"Please Select First Part (.1) To Merge");
            return;
        }
        int partCount=1;
        String name= file.getName();
        String outName=name.substring(0,name.length()-2);
        File outFile=new File(currentDir,outName);

        TaskDialog dlg=new TaskDialog("Merge Files");
        dlg.setMaximun(1);
        dlg.setVisible(true);
        int count=0;
        dlg.setValue(count);
        dlg.setLabel("<html><body>Merge Files:<br>"+file.getAbsolutePath()+"</body></html>");
        File openFolder=currentDir;


        try {
            BufferedOutputStream bOS=new BufferedOutputStream(new FileOutputStream(outFile));
            boolean loop=true;
            while (loop){
                dlg.setValue(++count);
                dlg.setMaximun(count+1);

                BufferedInputStream bIS=new BufferedInputStream(new FileInputStream(file));
                byte[] buffer=new byte[1024];
                int len=0;
                while ((len=bIS.read(buffer,0,1024)) > 0){
                    bOS.write(buffer,0,len);
                }
                partCount++;
                File nextPart=new File(currentDir,outName+"."+partCount);
                if(nextPart.exists()){
                    file=nextPart;
                }else{
                    loop=false;
                }
                bIS.close();
            }
            bOS.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        dlg.setValue(1);
        dlg.setMaximun(1);
        dlg.setLabel("<html><body>Completed Merge Files To:<br>" + outFile.getAbsolutePath()+"</body></html>");
        dlg.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        if(currentDir.getAbsolutePath().equals(openFolder.getAbsolutePath()))
            loadFilesList(currentDir.getAbsolutePath(),true);

    }

        @Override
    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        JTable table=fileManager.getTable();
        JButton renameButton=fileManager.getRenameButton();
        JButton copyButton=fileManager.getCopyButton();
        JButton zipButton=fileManager.getZipButton();
        JButton unZipButton=fileManager.getUnzipButton();
        JButton splitFile=fileManager.getSplitFileButton();
        JButton mergeButton=fileManager.getMergeFileButton();
        int selectRow=table.getSelectedRow();
        if( selectRow!= -1){
            renameButton.setEnabled(true);
            copyButton.setEnabled(true);
            zipButton.setEnabled(true);
            File selectFile=(File) table.getModel().getValueAt(selectRow,5);
            unZipButton.setEnabled(Untils.isZippedFile(selectFile));
            splitFile.setEnabled(selectFile.isFile());
            mergeButton.setEnabled(Untils.isSplittedFirstFile(selectFile));
        }else{
            renameButton.setEnabled(false);
            copyButton.setEnabled(false);
            unZipButton.setEnabled(false);
            splitFile.setEnabled(false);
            mergeButton.setEnabled(false);
            zipButton.setEnabled(false);
        }
    }
}
